/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala , Paul George
Email id: neelgala@gmail.com , command.paul@gmail.com
Details:
 NOC Layout
  Y/X  0   1   2   3
   0   T   H   H   T
   1   T   C   C  RAM
   2   T   C   C  UART
   3   T   H   H   T
--------------------------------------------------------------------------------------------------
*/
package Soc_network_on_chip;
  `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import GetPut :: * ;
  import Connectable :: * ;
  import Vector :: * ;
  import BRAMCore :: * ;
  import Clocks::*;

  import cclass :: * ;
  import llc_bank :: * ;
  import coherence_types :: * ;
  import common_types:: * ;
  import Soc_crossbar_types :: * ;
  `include "coherence.defines"
  import ShaktiLink_Types :: * ;
  import address_map::*;
  import ShaktiLink_NIC::*;
  import Semi_FIFOF :: * ;

// Shakti - Open Smart Imports
  import Types::*;
  import MessageTypes::*;
  import VirtualChannelTypes::*;
  import RoutingTypes::*;
  import CreditTypes::*;

  import Network::*;
  import CreditUnit::*;
  import StatLogger::*;
  import MonitorStats::*;

  import uart::*;
  import bram_slc ::*;
  import RS232_modified :: * ;
	


  interface Ifc_Soc_network_on_chip;
		interface RS232 uart_io;
  `ifdef rtldump
    interface Vector#(`NrCaches,Get#(DumpType)) io_dump;
  `endif
    interface Reset uncore_init_reset;
  endinterface
  

  /*doc:module: */
  (*synthesize*)
  module mk_Soc_network_on_chip (Ifc_Soc_network_on_chip);

    let defclock <- exposeCurrentClock;
    let defreset <- exposeCurrentReset;
    
    Reg#(Bool) uncore_inited <- mkReg(False);

    MakeResetIfc uncore_hold_reset <-mkReset(0,False,defclock);            // create a new reset for curr_clk
    Reset core_reset <- mkResetEither(uncore_hold_reset.new_rst,defreset); 
    //-RULE: Assert derived_reset when dm is inactive
    rule generate_derived_reset(!uncore_inited);
      uncore_hold_reset.assertReset;
    endrule

    Ifc_cclass_shl cores[`NrCaches];   
    for (Integer i = 0; i<`NrCaches; i = i + 1) begin
      cores[i] <- mkcclass_shl(`resetpc, fromInteger(i),reset_by core_reset);
    end
    
    Ifc_llc_bank#(`paddr, TDiv#(`linesize,8), FSize, FSize, SizeOf#(MessageType), 
      TLog#(`NrCaches),1, `dwords, `dblocks, `dsets, TAdd#(1, TMul#(`dways,`NrCaches))) llc[`NrCaches];
    for (Integer i = 0; i<`NrCaches; i = i + 1) begin
      llc[i] <- mkllc_bank_instance(fromInteger(i),reset_by core_reset);
    end
    // memory instance
    Ifc_bram_slc#(`paddr, TDiv#(`linesize,8), FSize, FSize, SizeOf#(MessageType), 
                          TLog#(`NrCaches),1) bram <- mkbram_slc("DDR",reset_by core_reset);
    Ifc_uart_slc#(`paddr, TDiv#(`linesize,8), FSize, FSize, SizeOf#(MessageType),
                          TLog#(`NrCaches),1,16) uart <- mkuart_slc(defclock, defreset, 2, 
                          pack(STOP_1), pack(ODD), "uart",reset_by core_reset);
    
    // Terminations at unused network on chip endpoints 6 Dead Nodes - 2 Dead Master Nodes. & at Bram & Uart Ports.
    Ifc_slc_slave#(`paddr, TDiv#(`linesize,8), FSize, FSize, SizeOf#(MessageType), TLog#(`NrCaches),1)  slc_slave_termination  [6];
    for (Integer i = 0; i < 6; i = i + 1) begin
      slc_slave_termination[i] <- mk_slave_port_termination("Slave_port_termination",i,reset_by core_reset);
    end
    Ifc_slc_master#(`paddr, TDiv#(`linesize,8), FSize, FSize, SizeOf#(MessageType), TLog#(`NrCaches),1) slc_master_termination [8];
    for (Integer i = 0; i < 8; i = i + 1) begin
      slc_master_termination[i] <- mk_master_port_termination("Master_port_termination" , i,reset_by core_reset);
    end

	//-------------------------- Network on Chip Test Shell------------------------------------- //
  Reg#(Data) initCount <- mkReg(0);

  //  Network on Chip  Fabric
  Network meshNtk <- mkNetwork;

  // NoC -Tile Link Transactors
  Vector#(MeshHeight,Vector#(MeshWidth,
  ShaktiLink_NoC_Endpoint#(`paddr, TDiv#(`linesize,8), FSize, FSize, SizeOf#(MessageType), TLog#(`NrCaches),1)
  )) v_node_shaktilink_endpoint = newVector;

    // House Keeping
  // TODO Why set inited true after this if condition?
  rule init(!uncore_inited);
    initCount <= initCount + 1;
    if(meshNtk.isInited && (initCount > 16*(fromInteger(valueOf(MeshHeight)) + fromInteger(valueOf(MeshWidth)))))  begin
      uncore_inited <= True;
    end
  endrule

  // Connect the Shakti-link transactor with each network port (network external interface)
  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
      v_node_shaktilink_endpoint[i][j] <- mkConnect_ShaktiLink_NOC(meshNtk.ntkPorts[i][j],i,j,reset_by core_reset);
    end
  end

  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
      rule injection_rate_gov(uncore_inited);
        v_node_shaktilink_endpoint[i][j].injection_rate_governor(True);
      endrule
    end
  end

  // ------------------------   Network on Chip Wiring  -------------------------------- //
  // Position 0   - ( 0,0 ) - Hart 0
  mkConnection(v_node_shaktilink_endpoint[0][0].node_master,cores[0].slave_side);  
  mkConnection(v_node_shaktilink_endpoint[0][0].node_slave ,cores[0].master_side);
  // Position 1   - ( 0,1 ) - Hart 1
  mkConnection(v_node_shaktilink_endpoint[0][1].node_master,cores[1].slave_side);  
  mkConnection(v_node_shaktilink_endpoint[0][1].node_slave ,cores[1].master_side);
  // Position 2   - ( 0,2 ) - Hart 2
  mkConnection(v_node_shaktilink_endpoint[0][2].node_master,cores[2].slave_side);  
  mkConnection(v_node_shaktilink_endpoint[0][2].node_slave ,cores[2].master_side);
  // Position 3   - ( 0,3 ) - Hart 3
  mkConnection(v_node_shaktilink_endpoint[0][3].node_master,cores[3].slave_side);  
  mkConnection(v_node_shaktilink_endpoint[0][3].node_slave ,cores[3].master_side);
  // Position 4   - ( 1,0 ) - LLC Bank 0
  mkConnection(v_node_shaktilink_endpoint[1][0].node_master,llc[0].slave_side);
  mkConnection(v_node_shaktilink_endpoint[1][0].node_slave ,llc[0].master_side);
  // Position 5   - ( 1,1 ) - LLC Bank 1 
  mkConnection(v_node_shaktilink_endpoint[1][1].node_master,llc[1].slave_side);
  mkConnection(v_node_shaktilink_endpoint[1][1].node_slave ,llc[1].master_side);
  // Position 6   - ( 1,2 ) - LLC Bank 2
  mkConnection(v_node_shaktilink_endpoint[1][2].node_master,llc[2].slave_side);
  mkConnection(v_node_shaktilink_endpoint[1][2].node_slave ,llc[2].master_side);
  // Position 7   - ( 1,3 ) - LLC Bank 3
  mkConnection(v_node_shaktilink_endpoint[1][3].node_master,llc[3].slave_side);
  mkConnection(v_node_shaktilink_endpoint[1][3].node_slave ,llc[3].master_side);
  // Position 8   - ( 2,0 ) - BRAM / Termination
  mkConnection(v_node_shaktilink_endpoint[2][0].node_master,bram.slave_side);
  mkConnection(v_node_shaktilink_endpoint[2][0].node_slave ,slc_master_termination[6] );
  // Position 9   - ( 2,1 ) - Uart / Termination
  mkConnection(v_node_shaktilink_endpoint[2][1].node_master,uart.slave);
  mkConnection(v_node_shaktilink_endpoint[2][1].node_slave ,slc_master_termination[7] );
  // Position 10  - ( 2,2 ) - Termination
  mkConnection(v_node_shaktilink_endpoint[2][2].node_master,slc_slave_termination [0] );
  mkConnection(v_node_shaktilink_endpoint[2][2].node_slave ,slc_master_termination[0] );
  // Position 11  - ( 2,3 ) - Termination
  mkConnection(v_node_shaktilink_endpoint[2][3].node_master,slc_slave_termination [1] );
  mkConnection(v_node_shaktilink_endpoint[2][3].node_slave ,slc_master_termination[1] );
  // Position 12  - ( 3,0 ) - Termination
  mkConnection(v_node_shaktilink_endpoint[3][0].node_master,slc_slave_termination [2] );
  mkConnection(v_node_shaktilink_endpoint[3][0].node_slave ,slc_master_termination[2] );
  // Position 13  - ( 3,1 ) - Termination
  mkConnection(v_node_shaktilink_endpoint[3][1].node_master,slc_slave_termination [3] );
  mkConnection(v_node_shaktilink_endpoint[3][1].node_slave ,slc_master_termination[3] );
  // Position 14  - ( 3,2 ) - Termination
  mkConnection(v_node_shaktilink_endpoint[3][2].node_master,slc_slave_termination [4] );
  mkConnection(v_node_shaktilink_endpoint[3][2].node_slave ,slc_master_termination[4] );
  // Position 15  - ( 3,3 ) - Termination
  mkConnection(v_node_shaktilink_endpoint[3][3].node_master,slc_slave_termination [5] );
  mkConnection(v_node_shaktilink_endpoint[3][3].node_slave ,slc_master_termination[5] );

  
  // ----------------------------------------------------------------------------------------- //
  
    Vector#(`NrCaches, Get#(DumpType)) lv_io_dump;
    for (Integer i = 0; i<`NrCaches; i = i + 1) begin
      lv_io_dump[i] = cores[i].io_dump;
    end
    interface uart_io = uart.io;
  `ifdef rtldump
    interface io_dump= lv_io_dump;
  `endif
    interface uncore_init_reset = core_reset;
  endmodule
  

  module mk_master_port_termination#(parameter String modulename , Integer source_id)(Ifc_slc_master#(a,w,o,i,op,acks,u));

    Ifc_slc_master_agent#(a,w,o,i,op,acks,u) sl_master_agent <- mkslc_master_agent();

    rule rl_drive_B;
      Fwd_channel#(a,w,o,i,op,u) grant_packet = sl_master_agent.o_fwd_channel.first();
      `logLevel( fabric, 0, $format("Packet Reached Termination ", fshow (grant_packet)))
      sl_master_agent.o_fwd_channel.deq();
    endrule

    rule rl_drive_C;
      Resp_channel#(a,w,o,i,op,acks,u) grant_packet = sl_master_agent.o_resp_channel.first();
      `logLevel( fabric, 0, $format("Packet Reached Termination ", fshow (grant_packet)))
      sl_master_agent.o_resp_channel.deq();
    endrule
    
    return sl_master_agent.shaktilink_side;

  endmodule

  module mk_slave_port_termination#(parameter String modulename , Integer source_id)(Ifc_slc_slave#(a,w,o,i,op,acks,u));

    Ifc_slc_slave_agent#(a,w,o,i,op,acks,u) sl_slave_agent <- mkslc_slave_agent();

    rule rl_drive_A;
      Req_channel#(a,w,o,i,op,u) grant_packet = sl_slave_agent.o_req_channel.first();
      `logLevel( fabric, 0, $format("Packet Reached Termination ", fshow (grant_packet)))
      sl_slave_agent.o_req_channel.deq();
    endrule

    return sl_slave_agent.shaktilink_side;

  endmodule
endpackage

