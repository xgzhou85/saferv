#include "util.h"
//#include <stdbool.h>
#define NUM_THREADS 4

// declaration and initial values of global variables
//extern volatile char Entering[NUM_THREADS]; 		//: array [1..NUM_THREADS] of bool = {false};
//extern volatile unsigned int Number[NUM_THREADS];   //: array [1..NUM_THREADS] of integer = {0};
extern volatile unsigned int  init_barrier;

void  main () {
	
	unsigned int hart_id = 0;
	__asm__ volatile("csrr %0, mhartid" : "=r" (hart_id));

  while(1) {
    if(init_barrier == hart_id) {
      printf("Hello from Hart:%d\n",hart_id);
      if(hart_id == 3)
        init_barrier = 0;
      else
        init_barrier = hart_id + 1;
    }
  }
}
