package tl_master;

  import StmtFSM :: * ;
  import GetPut::*;
  import FIFO::*;
  import FIFOF::*;
	import SpecialFIFOs :: * ;
	import Semi_FIFOF::*;
  import LFSR::*;
  import Tilelink_Types::*;
  import Vector::*; //for rotateBitsBy
  /*  
    Only one Request at a time , Blocking 
    Generates Request on A Channel
    Waits for response on D channel
    Sends E Channel
    Concurrently Listens on B channel 
    Responds on C Channel
    Done using Statement FSM
  
  Across the five channels, TL-C specifies ten messages comprising three operations.
    Message     Opcode      Operation     A   B   C   D   E     Response
    Acquire        6         Acquire      y   .   .   .   .     Grant, GrantData
    Grant          4         Acquire      .   .   .   y   .     GrantAck
    GrantData      5         Acquire      .   .   .   y   .     GrantAck
    GrantAck       -         Acquire      .   .   .   .   y
    Probe          6         Probe        .   y   .   .   .     ProbeAck, ProbeAckData
    ProbeAck       4         Probe        .   .   y   .   .
    ProbeAckData   5         Probe        .   .   y   .   .
    Release        6         Release      .   .   y   .   .     ReleaseAck
    ReleaseData    7         Release      .   .   y   .   .     ReleaseAck
    ReleaseAck     6         Release      .   .   .   y   .
    Table 8.2: Summary of TL-C Permission Transfer Operation Messages

  */

  interface Ifc_tl_master_tg#(numeric type a, numeric type w, numeric type z,numeric type o,numeric type i);
    method Action ma_start;
    method Action ma_finish;
		method Action ma_rand_value(Bit#(a) lfsr_value);
		interface Ifc_tlc_master#(a,w,z,o,i) tl_master;
    method Bool mv_done;
  endinterface


  //module parameter address map 
  (*synthesize*)
  module mk_instance_tl_master_tg(Empty);
    Ifc_tl_master_tg#(32, 64,32,2,2) instant <- mk_tl_master_tg('h0,TL_C);
  endmodule
 
	module mk_tl_master_tg#(Integer source_id ,TL_Capability capability)(Ifc_tl_master_tg#(a,w,z,o,i))
	provisos(Add#(a__, z, a));
    
    Reg#(Bool) rg_start <- mkReg(False);
    Reg#(Bool) rg_finish <- mkReg(False);
   
		// Master Agent 
		Ifc_tlc_master_agent#(a,w,z,o,i) tl_master_agent <- mktlc_master_agent();

    // Generate a Transaction Signature
    LFSR#(Bit#(4))  rand_event <- mkLFSR_4();
    // How to generate an address Generate any address , if it corresponds to the local id of the setup then 
    FIFO#(Bit#(a)) fifo_txn_signature <- mkSizedFIFO(2);

    Reg#(Bit#(z)) rg_beat_counter <- mkReg(0);
    Reg#(Bit#(a)) rg_acquire_address<- mkReg(unpack(0));
		Reg#(Bit#(z)) rg_acquire_size   <- mkReg(unpack(0));
		Reg#(Bit#(i))	rg_grant_ack_dest <- mkReg(unpack(0));
    Reg#(Bit#(a)) rg_release_address<- mkReg(unpack(0));
    Reg#(Bit#(z)) rg_release_size   <- mkReg(unpack(0));
		Reg#(Bool)    rg_wait_for_release_ack <- mkReg(False);

		// Start Sequence
		Reg#(Bool) rg_acquire_block <- mkReg(False);
		Reg#(Bool) rg_start_a <- mkReg(False);
		Reg#(Bool) rg_start_d <- mkReg(False);
		Reg#(Bool) rg_start_r <- mkReg(False);
		Reg#(Bool) rg_start_rd <- mkReg(False);
		Reg#(Bool) rg_start_e <- mkReg(False);
		Reg#(Bool) rg_wait_release_ack <- mkReg(False);

		// rule rl_display;
		// 	$display("SOURCE:: %x ::  %x,%x,%x,%x,%x,%x,%x",fromInteger(source_id),rg_acquire_block,rg_start_a,rg_start_d,rg_start_r,rg_start_rd,rg_start_e,rg_wait_release_ack);
		// endrule

		rule rl_drive_A (rg_start_a && (!rg_finish));
			let txn = fifo_txn_signature.first();
			A_channel#(a,w,z,o) packet_acquire = ?;
			packet_acquire.a_opcode = AcquireBlock;
			packet_acquire.a_size = truncate(txn);
			packet_acquire.a_address = txn;
			packet_acquire.a_source = fromInteger(source_id);
			fifo_txn_signature.deq();
			tl_master_agent.i_a_channel.enq(packet_acquire);
			//$display("%d",source_id,fshow(packet_acquire));
			rg_acquire_size <= truncate(txn); 
			rg_start_a <= False;
			rg_start_d <= True;
		endrule
		
		// Concurrent C - Channel handling 
		// C- Channel Release
	  // C -Channel Probe Acknolowdgement Handling
		rule rl_drive_C;
			let txn = fifo_txn_signature.first();
			//tl_master_agent.i_c_channel
			// look at pending blocking operation request bits.
			// Release in progress. - block till probe ack.
			// Probe Ack Data in progress - delay and wait on release. 
		endrule

		rule rl_drive_E (rg_start_e == True);
			E_channel#(i) grant_ack = ?;
			grant_ack.e_sink = rg_grant_ack_dest;
			$display("Grant Sent !",fshow(grant_ack));
			tl_master_agent.i_e_channel.enq(grant_ack);
			rg_start_e <= False;
			rg_acquire_block <= False;
		endrule

		// Concurrent D - Channel handling
		// D- Channel release response
		// D- Channel grant   response
		rule rl_drive_D(rg_start_d || rg_wait_release_ack );
			D_channel#(w,z,o,i) grant_packet = tl_master_agent.o_d_channel.first();
			if((grant_packet.d_opcode == GrantData) ||(grant_packet.d_opcode == Grant))begin 
				$display("%d",source_id,fshow(grant_packet));
				tl_master_agent.o_d_channel.deq();
				rg_grant_ack_dest <= grant_packet.d_sink;
				if(rg_acquire_size > 0 )
					rg_acquire_size <= rg_acquire_size - 1;
				else begin
					rg_start_e <= True;
					rg_start_d <= False;
				end
			end
			else if(grant_packet.d_opcode == ReleaseAck )begin
			
			end
		endrule
		//tl_master_agent.o_b_channel

		    // Start
    Reg#(Bit#(4)) rg_event <- mkReg(0);


    rule rl_reset_acquire_block ( ! rg_acquire_block && rg_start);
			rg_start_a <= True;
			rg_start_e <= False;
			rg_start_d <= False;
      rg_acquire_block <= True;
    endrule

  // Interfaces 
    method Action ma_start if(!rg_start);
      rg_start <= True;
      rand_event.seed(4'ha);
    endmethod
    method Action ma_finish if (rg_start);
      rg_finish <= True;
		endmethod
		method Action ma_rand_value(Bit#(a) lfsr_value) if(rg_start);
			Bit#(o) rot_val = fromInteger(source_id);
      fifo_txn_signature.enq(rotateBitsBy(lfsr_value,unpack(fromInteger(source_id))));
    endmethod
    method Bool mv_done if (rg_finish);
      return True;
    endmethod
    
    interface tl_master = tl_master_agent.tilelink_side;
  endmodule
endpackage :tl_master