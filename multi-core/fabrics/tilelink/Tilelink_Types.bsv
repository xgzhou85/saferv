/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala, Paul George, Rahul Bodduna
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package Tilelink_Types;
  `include "Logger.bsv"
  `include "system.defines"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import GetPut ::*;
  import Connectable ::*;
  import Semi_FIFOF :: *;

  // permissions
  typedef enum {NtoB=0, NtoT=1, BtoT=2} Grow deriving(Bits, FShow, Eq);
  typedef enum {TtoB=0, TtoN=1, BtoN=2} Prune deriving(Bits, FShow, Eq);
  typedef enum {TtoT=3, BtoB=4, NtoN=5} Report deriving(Bits, FShow, Eq);
  //typedef 3'd0 NtoB;
  //typedef 3'd1 NtoT;
  //typedef 3'd2 BtoT;
  //typedef 3'd0 TtoB;
  //typedef 3'd1 TtoN;
  //typedef 3'd2 BtoN;
  //typedef 3'd3 TtoT;
  //typedef 3'd4 BtoB;
  //typedef 3'd5 NtoN;

  typedef enum { TL_UL,
  							 TL_UH,
  							 TL_C } TL_Capability deriving (Bits , Eq, FShow );
  
  typedef enum {  PutFullData,
  								PutPartialData,
  								ArithmeticData,
  								LogicalData,
  								GetR,
  								Intent,
  								AcquireBlock,
  								AcquirePerm } A_Opcode deriving (Bits , Eq, FShow );
  
  typedef enum {  PutFullData,
  								PutPartialData,
  								ArithmeticData,
  								LogicalData,
  								GetR,
  								Intent,
  								ProbeBlock,
  								ProbePerm } B_Opcode deriving (Bits , Eq, FShow );
  
  typedef enum {  AccessAck,
  								AccessAckData,
  								HintAck,
  								ProbeAck,
  								ProbeAckData,
  								Release,
  								ReleaseData,
  								ProbeAckInv } C_Opcode deriving(Bits, Eq, FShow);
  
  typedef enum {	AccessAck,
  								AccessAckData,
  								HintAck,
  								Grant,
  								GrantData,
  								ReleaseAck } D_Opcode deriving(Bits, Eq, FShow);			
  
  // NOTE :: 			E_Opcode is implictly GrantAck
  
  typedef enum { 	Min,
  			   			 	Max,
  			   			 	MinU,
  			   			 	MaxU,
  			   			 	ADD } Param_arith deriving(Bits, Eq, FShow);
  
  typedef enum { 	Min,
  			   				Max,
  			   				MinU,
  			   				MaxU,
  			   				ADD } Param_logical deriving(Bits, Eq, FShow);
  
  
  typedef struct { 
  		A_Opcode 			    a_opcode;
  		Bit#(3)  		      a_param;
  		Bit#(z)				    a_size;
  		Bit#(o)           a_source;
  		Bit#(a)				    a_address;
  		Bit#(w)	          a_mask;
  		Bit#(TMul#(8,w))  a_data;	
  		Bit#(1)           a_corrupt;
  } A_channel#(numeric type a, numeric type w, numeric type z,numeric type o) deriving(Bits, Eq, FShow);
  
  //cache-coherence channels
  typedef struct {                                        
  		B_Opcode 			    b_opcode;
  		Bit#(3)  		      b_param;
  		Bit#(z)				    b_size;
  		Bit#(o)		        b_source;
  		Bit#(a)				    b_address;
  		Bit#(w)           b_mask;
  		Bit#(TMul#(w,8))  b_data;	
  		Bit#(1)           b_corrupt;
  } B_channel#(numeric type a, numeric type w, numeric type z,numeric type o) deriving(Bits, Eq, FShow);
  
  //cache-coherence channels
  typedef struct { 
  		C_Opcode 			    c_opcode;
  		Bit#(3)  		      c_param;
  		Bit#(z)				    c_size;
  		Bit#(o)           c_source;
  		Bit#(a)				    c_address;
  		Bit#(TMul#(w,8))  c_data;	
  		Bit#(1)           c_corrupt;
  } C_channel#(numeric type a, numeric type w, numeric type z,numeric type o) deriving(Bits, Eq, FShow);
  
  //The channel D is responsible for the slave responses. It has the master ids and slave ids carried through the channel
  typedef struct { 
  		D_Opcode		      d_opcode;                     //Opcode encodings for responses with data or just ack
  		Bit#(3)  		      d_param;
  		Bit#(z)			      d_size;
  		Bit#(o) 	        d_source;
  		Bit#(i)		        d_sink;
  		Bit#(TMul#(8,w))  d_data;	
  		Bit#(1)           d_corrupt;
  		Bit#(1)           d_denied;
  } D_channel#(numeric type w, numeric type z,numeric type o,numeric type i ) deriving(Bits, Eq, FShow);
  
  typedef struct { 
  		Bit#(i)		e_sink;
  } E_channel#(numeric type i) deriving(Bits, Eq,FShow);



  interface Ifc_tlc_master#(numeric type a, numeric type w, numeric type z, numeric type o, 
                                  numeric type i);
    // interface declaration for A_Channel
    (* always_ready, result="a_opcode" *) method Bit#(3) m_a_opcode; 
    (* always_ready, result="a_param"  *) method Bit#(3) m_a_param; 
    (* always_ready, result="a_size"   *) method Bit#(z) m_a_size; 
    (* always_ready, result="a_source" *) method Bit#(o) m_a_source; 
    (* always_ready, result="a_address"*) method Bit#(a) m_a_address; 
    (* always_ready, result="a_mask"   *) method Bit#(w) m_a_mask; 
    (* always_ready, result="a_data"   *) method Bit#(TMul#(w,8)) m_a_data; 
    (* always_ready, result="a_corrupt"*) method Bit#(1) m_a_corrupt;
    (* always_ready, result="a_valid"  *) method Bool m_a_valid;
    (* always_ready, always_enabled , prefix="" *) method Action m_a_ready ((* port="a_ready" *) Bool a_ready);    // in
    
    // interface declaration for B_Channel
    (* always_ready, always_enabled , prefix="" *) method Action m_b_valid (
      (* port="b_opcode" *) Bit#(3) b_opcode, 
      (* port="b_param"  *) Bit#(3) b_param, 
      (* port="b_size"   *) Bit#(z) b_size, 
      (* port="b_source" *) Bit#(o) b_source, 
      (* port="b_address"*) Bit#(a) b_address, 
      (* port="b_mask"   *) Bit#(w) b_mask, 
      (* port="b_data"   *) Bit#(TMul#(w,8)) b_data, 
      (* port="b_corrupt"*) Bit#(1) b_corrupt,
      (* port="b_valid"  *) Bool b_valid );
    (* always_ready, result="b_ready" *) method Bool m_b_ready;    
    
    // interface declaration for C_Channel
    (* always_ready, result="c_opcode" *) method Bit#(3) m_c_opcode; 
    (* always_ready, result="c_param"  *) method Bit#(3) m_c_param; 
    (* always_ready, result="c_size"   *) method Bit#(z) m_c_size; 
    (* always_ready, result="c_source" *) method Bit#(o) m_c_source; 
    (* always_ready, result="c_address"*) method Bit#(a) m_c_address; 
    (* always_ready, result="c_data"   *) method Bit#(TMul#(w,8)) m_c_data; 
    (* always_ready, result="c_corrupt"*) method Bit#(1) m_c_corrupt;
    (* always_ready, result="c_valid"  *) method Bool m_c_valid;
    (* always_ready, always_enabled , prefix="" *) method Action m_c_ready ((* port="c_ready" *) Bool c_ready);    // in
    
    // interface declaration for D_Channel
    (* always_ready, always_enabled , prefix="" *) method Action m_d_valid (
      (* port="d_opcode" *) Bit#(3) d_opcode, 
      (* port="d_param"  *) Bit#(3) d_param, 
      (* port="d_size"   *) Bit#(z) d_size, 
      (* port="d_source" *) Bit#(o) d_source, 
      (* port="d_sink"   *) Bit#(i) d_sink, 
      (* port="d_denied" *) Bit#(1) d_denied, 
      (* port="d_data"   *) Bit#(TMul#(w,8)) d_data, 
      (* port="d_corrupt"*) Bit#(1) d_corrupt,
      (* port="d_valid"  *) Bool d_valid );
    (* always_ready, result="d_ready" *) method Bool m_d_ready;    

    // interface declaration for E_Channel
    (* always_ready, result="e_sink" *) method Bit#(i) m_e_sink;
    (* always_ready, result="e_valid"*) method Bool m_e_valid;
    (* always_ready, always_enabled , prefix="" *) method Action m_e_ready ((* port="e_ready" *) Bool e_ready);    // in
  endinterface: Ifc_tlc_master
  
  interface Ifc_tlc_slave#(numeric type a, numeric type w, numeric type z, numeric type o, 
                                  numeric type i);
    // interface declaration for A_Channel
    (* always_ready, always_enabled , prefix="" *) method Action s_a_valid (
        (* port="a_opcode" *) Bit#(3) a_opcode, 
        (* port="a_param"  *) Bit#(3) a_param, 
        (* port="a_size"   *) Bit#(z) a_size, 
        (* port="a_source" *) Bit#(o) a_source, 
        (* port="a_address"*) Bit#(a) a_address, 
        (* port="a_mask"   *) Bit#(w) a_mask, 
        (* port="a_data"   *) Bit#(TMul#(w,8)) a_data, 
        (* port="a_corrupt"*) Bit#(1) a_corrupt,
        (* port="a_valid"  *) Bool a_valid);
    (* always_ready, result="a_ready" *) method Bool s_a_ready ;    // in
    
    // interface declaration for B_Channel
    (* always_ready, result="b_opcode" *) method Bit#(3) s_b_opcode; 
    (* always_ready, result="b_param"  *) method Bit#(3) s_b_param; 
    (* always_ready, result="b_size"   *) method Bit#(z) s_b_size; 
    (* always_ready, result="b_source" *) method Bit#(o) s_b_source; 
    (* always_ready, result="b_address"*) method Bit#(a) s_b_address; 
    (* always_ready, result="b_mask"   *) method Bit#(w) s_b_mask; 
    (* always_ready, result="b_data"   *) method Bit#(TMul#(w,8)) s_b_data;
    (* always_ready, result="b_corrupt"*) method Bit#(1) s_b_corrupt;
    (* always_ready, result="b_valid"  *) method Bool s_b_valid ;
    (* always_ready, always_enabled , prefix="" *) method Action s_b_ready ((* port="b_ready" *) Bool b_ready);    // in
    
    // interface declaration for C_Channel
    (* always_ready, always_enabled , prefix="" *) method Action s_c_valid (    // in
        (* port="c_opcode" *) Bit#(3) c_opcode, 
        (* port="c_param"  *) Bit#(3) c_param, 
        (* port="c_size"   *) Bit#(z) c_size, 
        (* port="c_source" *) Bit#(o) c_source, 
        (* port="c_address"*) Bit#(a) c_address, 
        (* port="c_data"   *) Bit#(TMul#(w,8)) c_data, 
        (* port="c_corrupt"*) Bit#(1) c_corrupt,
        (* port="c_valid"  *) Bool c_valid );
    (* always_ready, result="c_ready" *) method Bool s_c_ready;    // in
    
    // interface declaration for D_Channel
    (* always_ready, result="d_opcode" *) method Bit#(3) s_d_opcode; 
    (* always_ready, result="d_param"  *) method Bit#(3) s_d_param; 
    (* always_ready, result="d_size"   *) method Bit#(z) s_d_size; 
    (* always_ready, result="d_source" *) method Bit#(o) s_d_source; 
    (* always_ready, result="d_sink"   *) method Bit#(i) s_d_sink; 
    (* always_ready, result="d_denied" *) method Bit#(1) s_d_denied; 
    (* always_ready, result="d_data"   *) method Bit#(TMul#(w,8)) s_d_data;
    (* always_ready, result="d_corrupt"*) method Bit#(1) s_d_corrupt;
    (* always_ready, result="d_valid"  *) method Bool s_d_valid ;
    (* always_ready, always_enabled , prefix="" *) method Action s_d_ready ((* port="d_ready" *) Bool d_ready);    // in

    // interface declaration for E_Channel
    (* always_ready, always_enabled , prefix="" *) method Action s_e_valid (    // in
      (* port="e_sink" *) Bit#(i) e_sink,
      (* port="e_valid"*) Bool e_valid);
    (* always_ready, result="e_ready" *) method Bool s_e_ready ;    // in
  endinterface: Ifc_tlc_slave
  
  instance Connectable#(Ifc_tlc_slave#(a,w,z,o,i), Ifc_tlc_master#(a,w,z,o,i));
    
    module mkConnection#(Ifc_tlc_slave#(a,w,z,o,i) s, Ifc_tlc_master#(a,w,z,o,i) m)
		  (Empty);
		  mkConnection(m,s);
		endmodule
  endinstance

  instance Connectable#(Ifc_tlc_master#(a,w,z,o,i), Ifc_tlc_slave#(a,w,z,o,i));
    module mkConnection#(Ifc_tlc_master#(a,w,z,o,i) m, Ifc_tlc_slave#(a,w,z,o,i) s)
		       (Empty);
      
      (* fire_when_enabled, no_implicit_conditions *)
      /*doc:rule: */
      rule rl_a_channel_connect;
        s.s_a_valid(m.m_a_opcode, m.m_a_param, m.m_a_size, m.m_a_source, m.m_a_address, m.m_a_mask,
          m.m_a_data, m.m_a_corrupt, m.m_a_valid);
        m.m_a_ready(s.s_a_ready);
      endrule
      
      (* fire_when_enabled, no_implicit_conditions *)
      /*doc:rule: */
      rule rl_b_channel_connect;
        m.m_b_valid(s.s_b_opcode, s.s_b_param, s.s_b_size, s.s_b_source, s.s_b_address, s.s_b_mask,
          s.s_b_data, s.s_b_corrupt, s.s_b_valid);
        s.s_b_ready(m.m_b_ready);
      endrule

      /*doc:rule: */
      (* fire_when_enabled, no_implicit_conditions *)
      rule rl_c_channel_connet;
        s.s_c_valid(m.m_c_opcode, m.m_c_param, m.m_c_size, m.m_c_source, m.m_c_address, m.m_c_data,
          m.m_c_corrupt, m.m_c_valid);
        m.m_c_ready(s.s_c_ready);
      endrule

      (* fire_when_enabled, no_implicit_conditions *)
      /*doc:rule: */
      rule rl_d_channel_connect;
        m.m_d_valid(s.s_d_opcode, s.s_d_param, s.s_d_size, s.s_d_source, s.s_d_sink, s.s_d_denied,
          s.s_d_data, s.s_d_corrupt, s.s_d_valid);
        s.s_d_ready(m.m_d_ready);
      endrule

      (* fire_when_enabled, no_implicit_conditions *)
      /*doc:rule: */
      rule rl_e_channel_connect;
        s.s_e_valid(m.m_e_sink, m.m_e_valid);
        m.m_e_ready(s.s_e_ready);
      endrule
		endmodule
  endinstance


  interface Ifc_tlc_master_agent#(numeric type a, numeric type w, numeric type z, numeric type o, 
                                  numeric type i);

    interface Ifc_tlc_master#(a,w,z,o,i) tilelink_side;
    // FIFOF side
    interface FIFOF_I #(A_channel#(a,w,z,o))              i_a_channel;
    interface FIFOF_I #(C_channel#(a,w,z,o))              i_c_channel;
    interface FIFOF_I #(E_channel#(i))                    i_e_channel;

    interface FIFOF_O #(B_channel#(a,w,z,o))              o_b_channel;
    interface FIFOF_O #(D_channel#(w,z,o,i))              o_d_channel;
  endinterface: Ifc_tlc_master_agent

  /*doc:module: */
  module mktlc_master_agent (Ifc_tlc_master_agent#(a,w,z,o,i));

   Bool unguarded = True;
   Bool guarded   = False;

   // These FIFOs are guarded on BSV side, unguarded on AXI side
   FIFOF#(A_channel#(a,w,z,o)) f_a_channel <- mkGFIFOF (guarded, unguarded);
   FIFOF#(C_channel#(a,w,z,o)) f_c_channel <- mkGFIFOF (guarded, unguarded);
   FIFOF#(E_channel#(i))       f_e_channel <- mkGFIFOF (guarded, unguarded);
                              
   FIFOF#(B_channel#(a,w,z,o)) f_b_channel <- mkGFIFOF (unguarded, guarded);
   FIFOF#(D_channel#(w,z,o,i)) f_d_channel <- mkGFIFOF (unguarded, guarded);

   interface tilelink_side = interface Ifc_tlc_master
    // interface declaration for A_Channel
    method m_a_opcode =  pack(f_a_channel.first.a_opcode); 
    method m_a_param  =  f_a_channel.first.a_param; 
    method m_a_size   =  f_a_channel.first.a_size; 
    method m_a_source =  f_a_channel.first.a_source; 
    method m_a_address=  f_a_channel.first.a_address;
    method m_a_mask   =  f_a_channel.first.a_mask; 
    method m_a_data   =  f_a_channel.first.a_data;  
    method m_a_corrupt=  f_a_channel.first.a_corrupt;
    method m_a_valid = f_a_channel.notEmpty;
    method Action m_a_ready ( Bool a_ready);
      if(f_a_channel.notEmpty && a_ready) f_a_channel.deq;
    endmethod
    
    // interface declaration for B_Channel
    method Action m_b_valid ( Bit#(3) b_opcode, Bit#(3) b_param, Bit#(z) b_size, Bit#(o) b_source, 
                              Bit#(a) b_address, Bit#(w) b_mask, Bit#(TMul#(w,8)) b_data, 
                              Bit#(1) b_corrupt, Bool b_valid );
      if(f_b_channel.notFull && b_valid)
        f_b_channel.enq(B_channel{b_opcode: unpack(b_opcode), b_param: b_param, b_size: b_size, 
                                    b_source: b_source, b_address: b_address, b_mask: b_mask,
                                    b_data: b_data, b_corrupt: b_corrupt});
    endmethod
    method m_b_ready = f_b_channel.notFull;    

    // interface declaration for C_Channel
    method m_c_opcode   = pack(f_c_channel.first.c_opcode);
    method m_c_param    = f_c_channel.first.c_param; 
    method m_c_size     = f_c_channel.first.c_size; 
    method m_c_source   = f_c_channel.first.c_source; 
    method m_c_address  = f_c_channel.first.c_address; 
    method m_c_data     = f_c_channel.first.c_data; 
    method m_c_corrupt  = f_c_channel.first.c_corrupt;
    method m_c_valid    = f_c_channel.notEmpty;
    method Action m_c_ready (Bool c_ready);    // in
      if(f_c_channel.notEmpty && c_ready) f_c_channel.deq;
    endmethod
    
    // interface declaration for D_Channel
    method Action m_d_valid ( Bit#(3) d_opcode, Bit#(3) d_param, Bit#(z) d_size, Bit#(o) d_source, 
                              Bit#(i) d_sink, Bit#(1) d_denied, Bit#(TMul#(w,8)) d_data, 
                              Bit#(1) d_corrupt, Bool d_valid );
      if(f_d_channel.notFull && d_valid)
        f_d_channel.enq(D_channel{d_opcode: unpack(d_opcode), d_param: d_param, d_size: d_size, 
                                  d_source: d_source, d_sink: d_sink, d_denied: d_denied, 
                                  d_data: d_data, d_corrupt: d_corrupt});
    endmethod
    method Bool m_d_ready = f_d_channel.notFull;    

    // interface declaration for E_Channel
    method m_e_sink   = f_e_channel.first.e_sink;
    method m_e_valid  = f_e_channel.notEmpty;
    method Action m_e_ready (Bool e_ready);    // in
      if(f_e_channel.notEmpty && e_ready) f_e_channel.deq;
    endmethod
   endinterface;

   interface i_a_channel = to_FIFOF_I(f_a_channel);
   interface i_c_channel = to_FIFOF_I(f_c_channel);
   interface i_e_channel = to_FIFOF_I(f_e_channel);
   interface o_b_channel = to_FIFOF_O(f_b_channel);
   interface o_d_channel = to_FIFOF_O(f_d_channel);
  endmodule

  interface Ifc_tlc_slave_agent#(numeric type a, numeric type w, numeric type z, numeric type o, 
                                  numeric type i);

    interface Ifc_tlc_slave#(a,w,z,o,i) tilelink_side;
    // FIFOF side
    interface FIFOF_O #(A_channel#(a,w,z,o))              o_a_channel;
    interface FIFOF_O #(C_channel#(a,w,z,o))              o_c_channel;
    interface FIFOF_O #(E_channel#(i))                    o_e_channel;

    interface FIFOF_I #(B_channel#(a,w,z,o))              i_b_channel;
    interface FIFOF_I #(D_channel#(w,z,o,i))              i_d_channel;
  endinterface: Ifc_tlc_slave_agent
// Add an interface & Connectable type to bringout the ready valid signals for off chip
  /*doc:module: */
  module mktlc_slave_agent (Ifc_tlc_slave_agent#(a,w,z,o,i));
   
    Bool unguarded = True;
    Bool guarded   = False;

    // These FIFOs are guarded on BSV side, unguarded on AXI side
    FIFOF#(A_channel#(a,w,z,o)) f_a_channel <- mkGFIFOF (unguarded, guarded);
    FIFOF#(C_channel#(a,w,z,o)) f_c_channel <- mkGFIFOF (unguarded, guarded);
    FIFOF#(E_channel#(i))       f_e_channel <- mkGFIFOF (unguarded, guarded);
                               
    FIFOF#(B_channel#(a,w,z,o)) f_b_channel <- mkGFIFOF (guarded, unguarded);
    FIFOF#(D_channel#(w,z,o,i)) f_d_channel <- mkGFIFOF (guarded, unguarded);

    interface tilelink_side = interface Ifc_tlc_slave
      
      // interface declaration for A_Channel
      method Action s_a_valid (Bit#(3) a_opcode, Bit#(3) a_param, Bit#(z) a_size, Bit#(o) a_source, 
                              Bit#(a) a_address, Bit#(w) a_mask, Bit#(TMul#(w,8)) a_data, 
                              Bit#(1) a_corrupt, Bool a_valid);
        if(f_a_channel.notFull && a_valid)
          f_a_channel.enq(A_channel{a_opcode: unpack(a_opcode), a_param: a_param, a_size: a_size,
                                    a_source: a_source, a_address: a_address, a_mask: a_mask, 
                                    a_data: a_data, a_corrupt: a_corrupt});
      endmethod
      method s_a_ready = f_a_channel.notFull;    // in
      
      // interface declaration for B_Channel
      method s_b_opcode = pack(f_b_channel.first.b_opcode);
      method s_b_param = f_b_channel.first.b_param;
      method s_b_size = f_b_channel.first.b_size; 
      method s_b_source = f_b_channel.first.b_source; 
      method s_b_address = f_b_channel.first.b_address;
      method s_b_mask = f_b_channel.first.b_mask; 
      method s_b_data = f_b_channel.first.b_data;
      method s_b_corrupt = f_b_channel.first.b_corrupt;
      method s_b_valid = f_b_channel.notEmpty;
      method Action s_b_ready ( Bool b_ready);    // in
        if(b_ready && f_b_channel.notEmpty)
          f_b_channel.deq;
      endmethod
      
      // interface declaration for C_Channel
      method Action s_c_valid (Bit#(3) c_opcode, Bit#(3) c_param, Bit#(z) c_size, Bit#(o) c_source, 
                                Bit#(a) c_address, Bit#(TMul#(w,8)) c_data, Bit#(1) c_corrupt, 
                                Bool c_valid );
        if(f_c_channel.notFull && c_valid)
          f_c_channel.enq(C_channel{c_opcode: unpack(c_opcode), c_param: c_param, c_size: c_size,
                                    c_source: c_source, c_address: c_address, c_data: c_data, 
                                    c_corrupt: c_corrupt});
      endmethod
      method Bool s_c_ready = f_c_channel.notFull;
      
      // interface declaration for D_Channel
      method s_d_opcode = pack(f_d_channel.first.d_opcode);
      method s_d_param = f_d_channel.first.d_param;
      method s_d_size = f_d_channel.first.d_size;
      method s_d_source = f_d_channel.first.d_source;
      method s_d_sink = f_d_channel.first.d_sink;
      method s_d_denied = f_d_channel.first.d_denied;
      method s_d_data = f_d_channel.first.d_data;
      method s_d_corrupt = f_d_channel.first.d_corrupt;
      method s_d_valid  = f_d_channel.notEmpty;
      method Action s_d_ready (Bool d_ready);    // in
        if(d_ready && f_d_channel.notEmpty)
          f_d_channel.deq;
      endmethod

      // interface declaration for E_Channel
      method Action s_e_valid (Bit#(i) e_sink, Bool e_valid);
        if(e_valid && f_e_channel.notFull)
          f_e_channel.enq(E_channel{e_sink: e_sink});
      endmethod

      method s_e_ready = f_e_channel.notFull;    // in
    endinterface;

    interface o_a_channel = to_FIFOF_O(f_a_channel);
    interface o_c_channel = to_FIFOF_O(f_c_channel);
    interface o_e_channel = to_FIFOF_O(f_e_channel);
    interface i_b_channel = to_FIFOF_I(f_b_channel);
    interface i_d_channel = to_FIFOF_I(f_d_channel);
    
  endmodule
endpackage

