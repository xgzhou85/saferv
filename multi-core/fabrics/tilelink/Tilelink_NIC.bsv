/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
	This Package Provides Interfaces to Map Tile Link Messages to 
	flits on The Shakti - Open Smart NOC

	Dependencies :: Shakti Open Smart Libraries 
*/


package Tilelink_NIC;

`include "system.defines"
import GetPut ::*;
import FIFO ::*;
import FIFOF::*;
import SpecialFIFOs ::*;
import Semi_FIFOF::*;
import Connectable ::*;
import Vector::*;

import MessageTypes ::*;
import Types ::*;
import RoutingTypes ::*;
import CreditUnit::*;
import SmartVCAllocUnit::*;
import CreditTypes::*;
import Network::*;
import NIC_interface::*;
//import StatLogger::*;

import Tilelink_Types::*;
import address_map ::*;

typedef enum{
TL_Channel_A,
TL_Channel_B,
TL_Channel_C,
TL_Channel_D,
TL_Channel_E,
SysManagement } VirtualNetworks deriving (Bits , Eq , FShow);

typedef struct{
	Len node;
	Bit#(a) address;
	Bit#(w) data;
	Bool operation;
} Sys_mon_type#(numeric type a,numeric type w) deriving (Bits,Eq,FShow);

interface TL_NoC_Endpoint#(numeric type a,numeric type w,numeric type z,numeric type o,numeric type i);
	interface Ifc_tlc_master #(a,w,z,o,i) node_master;
	interface Ifc_tlc_slave  #(a,w,z,o,i) node_slave ;
	interface Get#(Sys_mon_type#(a,w)) sysmon_out;
	interface Put#(Sys_mon_type#(a,w)) sysmon_in;
	method Action injection_rate_governor(Bool rules_can_fire);
	method Action local_criticality (Bool criticality);
endinterface


(* descending_urgency = "rl_transfer_m_E,rl_transfer_s_D,rl_transfer_m_A,rl_transfer_m_SysMon"*)
(* descending_urgency = "rl_transfer_s_E,rl_transfer_m_D,rl_transfer_s_A,rl_transfer_s_SysMon"*)
module mkConnect_TL_NOC#(NetworkOuterInterface mesh,Integer node_id_y , Integer node_id_x )(TL_NoC_Endpoint#(a,w,z,o,i))
												provisos(	Add#(a__, TAdd#(3, TAdd#(3, TAdd#(z, TAdd#(o, TAdd#(i, TAdd#(TMul#(8, w),2)))))), 128),
												Add#(b__, TAdd#(3, TAdd#(3, TAdd#(z, TAdd#(o, TAdd#(a, TAdd#(w,TAdd#(TMul#(8, w), 1))))))), 128),
												Add#(c__, i, 128),
												Add#(d__, TAdd#(6, TAdd#(a, TAdd#(w, 1))), 128)
												);// TODO :: Rewrite Provisos to Human Readable and Not Configuration Sensitive

		// Transactors
		Ifc_tlc_master_agent#(a,w,z,o,i) tl_master_agent<- mktlc_master_agent();
		Ifc_tlc_slave_agent #(a,w,z,o,i) tl_slave_agent <- mktlc_slave_agent ();

		FIFOF#(Sys_mon_type#(a,w)) fifo_m_sys_mon <- mkFIFOF1();
		FIFOF#(Sys_mon_type#(a,w)) fifo_s_sys_mon <- mkFIFOF1();

		Reg#(Bit#(1)) crit <- mkDWire(0);
		Wire#(Bool) rules_can_fire <- mkDWire(False);

		////// - Credit & Credit Link Management

		SmartVCAllocUnit   vcAllocUnit 	<- mkSmartVCAllocUnit;
		ReverseCreditUnit  creditUnit <- mkReverseCreditUnit;

		Integer max_r_fifo_depth = valueOf(TExp#(o));// Maximum number of outstanding Request flits for a channel which are not being serviced + 1
		Vector#(NumVNETs,FIFOF#(Flit)) f_recieve_flit <- replicateM(mkSizedBypassFIFOF(max_r_fifo_depth)); 

		rule rl_input_credit_connect;
			let credit <- creditUnit.getCredit;
			mesh.putCredit(credit);
		endrule

		rule rl_output_credit_connect;
			CreditSignal credit <- mesh.getCredit;
			if(isValid(credit))begin
					vcAllocUnit.putFreeVC(validValue(credit).msgType,validValue(credit).critType, validValue(credit).vc);
			end
		endrule
		
		////// Data & Data Link Management

		rule rl_transfer_m_A(rules_can_fire);
			A_channel#(a,w,z,o) lv_flit_data = tl_slave_agent.o_a_channel.first();
			Bit#(DataSz) data = zeroExtend(pack(lv_flit_data));
			// Address to Destination
			Bit#(i) tl_dest = address_map(lv_flit_data.a_address);
			Len dest_coord = fn_slave_to_coordinates(tl_dest);
			LookAheadRouteInfo routeInfo = fn_encode_routing_info( Len {x_len:fromInteger(node_id_x) ,y_len:fromInteger(node_id_y) },dest_coord );
			$display(fshow(tl_dest),fshow(dest_coord),fshow(routeInfo),fshow(Len {x_len:fromInteger(node_id_x) ,y_len:fromInteger(node_id_y) }));
			Flit flit = Flit { msgType : pack(TL_Channel_A),
													critType : crit,
													vc : ?,
													flitType : HeadTail,
													routeInfo : routeInfo,
													flitData : data};
			flit.vc <- vcAllocUnit.getNextVC(flit.msgType,flit.critType );
			mesh.putFlit(flit);
			tl_slave_agent.o_a_channel.deq();
		endrule
		rule rl_transfer_m_E(rules_can_fire);
			E_channel#(i) 	lv_flit_data = tl_slave_agent.o_e_channel.first();
			Bit#(DataSz) data = zeroExtend(pack(lv_flit_data));
			// Destination to Coords
			Len dest_coord = fn_slave_to_coordinates(lv_flit_data.e_sink);
			LookAheadRouteInfo routeInfo = fn_encode_routing_info( Len {x_len:fromInteger(node_id_x) ,y_len:fromInteger(node_id_y) },dest_coord);
			Flit flit = Flit { msgType : pack(TL_Channel_E),
													critType : crit,
													vc : ?,
													flitType : HeadTail,
													routeInfo : routeInfo,
													flitData : data};
			flit.vc <- vcAllocUnit.getNextVC(flit.msgType,flit.critType );
			mesh.putFlit(flit);
			tl_slave_agent.o_e_channel.deq();
		endrule
		rule rl_transfer_s_D(rules_can_fire);
			D_channel#(w,z,o,i) lv_flit_data = tl_master_agent.o_d_channel.first();
			Bit#(DataSz) data = zeroExtend(pack(lv_flit_data));
			// dest to coords
			Len dest_coord = fn_master_to_coordinates(lv_flit_data.d_source);
			LookAheadRouteInfo routeInfo = fn_encode_routing_info( Len {	x_len:fromInteger(node_id_x) ,y_len:fromInteger(node_id_y) },dest_coord);
			Flit flit = Flit { msgType : pack(TL_Channel_D),
													critType : crit,
													vc : ?,
													flitType : HeadTail,
													routeInfo : routeInfo,
													flitData : data};
			flit.vc <- vcAllocUnit.getNextVC(flit.msgType,flit.critType);
			mesh.putFlit(flit);
			tl_master_agent.o_d_channel.deq();
		endrule
		rule rl_transfer_m_SysMon(rules_can_fire);
			Sys_mon_type#(a,w) lv_flit_data = fifo_m_sys_mon.first();
			Bit#(DataSz) data = zeroExtend(pack(lv_flit_data));
			// dest to coords
			LookAheadRouteInfo routeInfo = fn_encode_routing_info(Len {	x_len:fromInteger(node_id_x) ,y_len:fromInteger(node_id_y) },
																																	lv_flit_data.node);
			Flit flit = Flit { msgType : pack(SysManagement),
													critType : crit,
													vc : ?,
													flitType : HeadTail,
													routeInfo : routeInfo,
													flitData : data};
			flit.vc <- vcAllocUnit.getNextVC(flit.msgType,flit.critType );
			mesh.putFlit(flit);
			//$display("S S\t\t",fshow(flit));
			fifo_m_sys_mon.deq();
		endrule

		rule rl_buffer_incoming_flits;
			Flit flit <- mesh.getFlit();
			creditUnit.putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType}));
			f_recieve_flit[flit.msgType].enq(flit);
		endrule

		rule rl_transfer_s_A(rules_can_fire);
			Flit flit = f_recieve_flit[pack(TL_Channel_A)].first();
			A_channel#(a,w,z,o) lv_flit_data = unpack(truncate(flit.flitData));
			tl_master_agent.i_a_channel.enq(lv_flit_data);
			f_recieve_flit[pack(TL_Channel_A)].deq();
		endrule
		rule rl_transfer_s_E(rules_can_fire);
			Flit flit = f_recieve_flit[pack(TL_Channel_E)].first();
			creditUnit.putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType}));
			E_channel#(i) lv_flit_data = unpack(truncate(flit.flitData));
			tl_master_agent.i_e_channel.enq(lv_flit_data);
			f_recieve_flit[pack(TL_Channel_E)].deq();
		endrule
		rule rl_transfer_m_D(rules_can_fire);
			Flit flit = f_recieve_flit[pack(TL_Channel_D)].first();
			creditUnit.putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType}));
			D_channel#(w,z,o,i) lv_flit_data = unpack(truncate(flit.flitData));
			tl_slave_agent.i_d_channel.enq(lv_flit_data);
			f_recieve_flit[pack(TL_Channel_D)].deq();
		endrule
		rule rl_transfer_s_SysMon(rules_can_fire);
			Flit flit = f_recieve_flit[pack(SysManagement)].first();
			creditUnit.putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType}));
			Sys_mon_type#(a,w) lv_flit_data = unpack(truncate(flit.flitData));
			fifo_s_sys_mon.enq(lv_flit_data);
			f_recieve_flit[pack(SysManagement)].deq();
		endrule

		interface node_master = tl_master_agent.tilelink_side;
		interface node_slave  = tl_slave_agent.tilelink_side;
		interface sysmon_out = toGet(fifo_m_sys_mon);
		interface sysmon_in = toPut(fifo_s_sys_mon);

		method Action injection_rate_governor(Bool arg_rules_can_fire);
			rules_can_fire <= arg_rules_can_fire;
		endmethod
		method Action local_criticality (Bool criticality);
			if (criticality)
				crit <= 1'b1;
		endmethod

	endmodule
endpackage
