/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package ShaktiLink_Types;
  `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import Connectable :: * ;
  import Semi_FIFOF :: * ;
  
  typedef struct { 
    Bit#(op)  opcode; 
    Bit#(8)   len;		                           
    Bit#(3)   size;			                       
    Bit#(1)   tid;			                       
    Bit#(o)   source; 
    Bit#(i)   dest; 
    Bit#(a)   address; 
    Bit#(w)   mask; 
    Bit#(TMul#(w,8)) data; 
    Bit#(u) user;
  } Req_channel#(numeric type a, numeric type w,numeric type o, numeric type i, numeric type op,
               numeric type u) deriving(Bits, Eq, FShow);
  
  typedef struct {
    Bit#(op) opcode; 
    Bit#(o) source; 
    Bit#(i) dest; 
    Bit#(a) address; 
    Bit#(w) mask; 
    Bit#(TMul#(w,8)) data; 
    Bit#(1)   tid;			                       
    Bit#(u) user;
  } Fwd_channel#(numeric type a, numeric type w,numeric type o, numeric type i, numeric type op,
               numeric type u) deriving(Bits, Eq, FShow);

  typedef struct {
    Bit#(op) opcode; 
    Bit#(acks) acksExpected;
    Bool    last; 
    Bit#(o) source; 
    Bit#(i) dest; 
    Bit#(a) address; 
    Bit#(TMul#(w,8)) data; 
    Bit#(1)   tid;			                       
    Bit#(1) corrupt;
    Bit#(u) user; 
  } Resp_channel#(numeric type a, numeric type w,numeric type o, numeric type i, numeric type op,
               numeric type acks, numeric type u) deriving(Bits, Eq, FShow);

  interface Ifc_slc_master#(numeric type a, numeric type w, numeric type o, 
                            numeric type i, numeric type op, numeric type acks, numeric type u);

    // interface declaration for Req_Channel
    (* always_ready, result="req_opcode" *) method Bit#(op) m_req_opcode; 
    (* always_ready, result="req_len" *)    method Bit#(8) m_req_len;		                           
    (* always_ready, result="req_size" *)   method Bit#(3) m_req_size;			                       
    (* always_ready, result="req_tid" *)    method Bit#(1) m_req_tid;			                       
    (* always_ready, result="req_source" *) method Bit#(o) m_req_source; 
    (* always_ready, result="req_dest" *)   method Bit#(i) m_req_dest; 
    (* always_ready, result="req_address"*) method Bit#(a) m_req_address; 
    (* always_ready, result="req_mask"   *) method Bit#(w) m_req_mask; 
    (* always_ready, result="req_data"   *) method Bit#(TMul#(w,8)) m_req_data; 
    (* always_ready, result="req_user"   *) method Bit#(u) m_req_user;
    (* always_ready, result="req_valid"  *) method Bool m_req_valid;
    (* always_ready, always_enabled , prefix="" *) method Action m_req_ready ((* port="req_ready" *)
                                                        Bool req_ready);    // in
    
    // interface declaration for Fwd_Channel
    (* always_ready, always_enabled , prefix="" *) method Action m_fwd_valid(
      (* port="fwd_opcode" *) Bit#(op) fwd_opcode, 
      (* port="fwd_source" *) Bit#(o) fwd_source, 
      (* port="fwd_dest" *)   Bit#(i) fwd_dest, 
      (* port="fwd_address"*) Bit#(a) fwd_address, 
      (* port="fwd_mask"   *) Bit#(w) fwd_mask, 
      (* port="fwd_data"   *) Bit#(TMul#(w,8)) fwd_data, 
      (* port="fwd_tid" *)    Bit#(1) fwd_tid,			                       
      (* port="fwd_user"   *) Bit#(u) fwd_user,
      (* port="fwd_valid"  *) Bool fwd_valid);
    (* always_ready, result="fwd_ready" *) method Bool m_fwd_ready; 

    // interface declaration for Response_Channel
    (* always_ready, always_enabled , prefix="" *) method Action m_resp_valid (
      (* port="resp_opcode" *)        Bit#(op) resp_opcode, 
      (* port="resp_acksexpected" *)  Bit#(acks) resp_acksExpected,
      (* port="resp_last"   *)        Bool    resp_last, 
      (* port="resp_source" *)        Bit#(o) resp_source, 
      (* port="resp_dest" *)          Bit#(i) resp_dest, 
      (* port="resp_address"*)        Bit#(a) resp_address, 
      (* port="resp_data"   *)        Bit#(TMul#(w,8)) resp_data, 
      (* port="resp_tid" *)           Bit#(1) resp_tid,			                       
      (* port="resp_corrupt"*)        Bit#(1) resp_corrupt,
      (* port="resp_user"*)           Bit#(u) resp_user, 
      (* port="resp_valid"  *)        Bool resp_valid );
    (* always_ready, result="resp_ready" *) method Bool m_resp_ready;    
  endinterface
  interface Ifc_slc_slave#(numeric type a, numeric type w, numeric type o, 
                            numeric type i, numeric type op, numeric type acks, numeric type u);

    // interface declaration for Req_Channel
    (* always_ready, always_enabled , prefix="" *) method Action s_req_valid(
      (* port="req_opcode" *) Bit#(op) req_opcode, 
      (* port="req_len" *)    Bit#(8) req_len,		                           
      (* port="req_size" *)   Bit#(3) req_size,			                       
      (* port="req_source" *) Bit#(o) req_source, 
      (* port="req_dest" *)   Bit#(i) req_dest, 
      (* port="req_address"*) Bit#(a) req_address, 
      (* port="req_mask"   *) Bit#(w) req_mask, 
      (* port="req_data"   *) Bit#(TMul#(w,8)) req_data, 
      (* port="req_tid" *)    Bit#(1) req_tid,			                       
      (* port="req_user"   *) Bit#(u) req_user,
      (* port="req_valid"  *) Bool req_valid);
    (* always_ready, result="req_ready" *) method Bool s_req_ready;
    
    // interface declaration for Fwd_Channel
    (* always_ready, result="fwd_opcode" *) method Bit#(op) s_fwd_opcode; 
    (* always_ready, result="fwd_source" *) method Bit#(o) s_fwd_source; 
    (* always_ready, result="fwd_dest" *)   method Bit#(i) s_fwd_dest; 
    (* always_ready, result="fwd_address"*) method Bit#(a) s_fwd_address; 
    (* always_ready, result="fwd_mask"   *) method Bit#(w) s_fwd_mask; 
    (* always_ready, result="fwd_data"   *) method Bit#(TMul#(w,8)) s_fwd_data; 
    (* always_ready, result="fwd_tid" *)    method Bit#(1) s_fwd_tid;			                       
    (* always_ready, result="fwd_user"   *) method Bit#(u) s_fwd_user;
    (* always_ready, result="fwd_valid"  *) method Bool s_fwd_valid;
    (* always_ready, always_enabled , prefix="" *) method Action s_fwd_ready ((* port="fwd_ready" *)
                                                        Bool fwd_ready);    // in

    // interface declaration for Response_Channel
      (* always_ready, result="resp_opcode" *)        method Bit#(op) s_resp_opcode; 
      (* always_ready, result="resp_acksexpected" *)  method Bit#(acks) s_resp_acksExpected;
      (* always_ready, result="resp_last"   *)        method Bool    s_resp_last; 
      (* always_ready, result="resp_source" *)        method Bit#(o) s_resp_source; 
      (* always_ready, result="resp_dest" *)          method Bit#(i) s_resp_dest; 
      (* always_ready, result="resp_address"*)        method Bit#(a) s_resp_address; 
      (* always_ready, result="resp_data"   *)        method Bit#(TMul#(w,8)) s_resp_data; 
      (* always_ready, result="resp_tid" *)           method Bit#(1) s_resp_tid;			                       
      (* always_ready, result="resp_corrupt"*)        method Bit#(1) s_resp_corrupt;
      (* always_ready, result="resp_user"*)           method Bit#(u) s_resp_user; 
      (* always_ready, result="resp_valid"  *)        method Bool s_resp_valid ;
      (* always_ready, always_enabled , prefix="" *) method Action s_resp_ready ((*port =
                                                                  "resp_ready" *) Bool resp_ready);
  endinterface
  
  instance Connectable#(Ifc_slc_slave#(a,w,o,i,op,acks,u), Ifc_slc_master#(a,w,o,i,op,acks,u));
    
    module mkConnection#(Ifc_slc_slave#(a,w,o,i,op,acks,u) s, Ifc_slc_master#(a,w,o,i,op,acks,u) m)
		  (Empty);
		  mkConnection(m,s);
		endmodule
  endinstance

  instance Connectable#(Ifc_slc_master#(a,w,o,i,op,acks,u), Ifc_slc_slave#(a,w,o,i,op,acks,u));
    module mkConnection#(Ifc_slc_master#(a,w,o,i,op,acks,u) m, Ifc_slc_slave#(a,w,o,i,op,acks,u) s)
		       (Empty);
      
      (* fire_when_enabled, no_implicit_conditions *)
      /*doc:rule: */
      rule rl_req_channel_connect;
        s.s_req_valid(m.m_req_opcode, m.m_req_len, m.m_req_size, m.m_req_source, 
                      m.m_req_dest, m.m_req_address, m.m_req_mask, m.m_req_data, m.m_req_tid, 
                      m.m_req_user, m.m_req_valid);
        m.m_req_ready(s.s_req_ready);
      endrule
      
      /*doc:rule: */
      (* fire_when_enabled, no_implicit_conditions *)
      rule rl_c_channel_connet;
        m.m_fwd_valid(s.s_fwd_opcode, s.s_fwd_source, s.s_fwd_dest, s.s_fwd_address, s.s_fwd_mask,
                      s.s_fwd_data, s.s_fwd_tid, s.s_fwd_user, s.s_fwd_valid);
        s.s_fwd_ready(m.m_fwd_ready);
      endrule


      (* fire_when_enabled, no_implicit_conditions *)
      /*doc:rule: */
      rule rl_resp_channel_connect;
        m.m_resp_valid(s.s_resp_opcode, s.s_resp_acksExpected, s.s_resp_last, s.s_resp_source, 
                      s.s_resp_dest, s.s_resp_address, s.s_resp_data, s.s_resp_tid, s.s_resp_corrupt, 
                      s.s_resp_user, s.s_resp_valid );
        s.s_resp_ready(m.m_resp_ready);
      endrule
		endmodule
  endinstance

  interface Ifc_slc_master_agent#(numeric type a, numeric type w, numeric type o, 
                            numeric type i, numeric type op, numeric type acks, numeric type u);

    interface Ifc_slc_master#(a,w,o,i,op,acks,u) shaktilink_side;
    // FIFOF side
    interface FIFOF_I #(Req_channel#(a,w,o,i,op,u))              i_req_channel;
    interface FIFOF_O #(Fwd_channel#(a,w,o,i,op,u))              o_fwd_channel;
    interface FIFOF_O #(Resp_channel#(a,w,o,i,op,acks,u))        o_resp_channel;
  endinterface: Ifc_slc_master_agent

  module mkslc_master_agent (Ifc_slc_master_agent#(a,w,o,i,op,acks,u));

    Bool unguarded = True;
    Bool guarded   = False;

    // These FIFOs are guarded on BSV side, unguarded on AXI side
    FIFOF#(Req_channel#(a,w,o,i,op,u)) f_req_channel <- mkGFIFOF (guarded, unguarded);

    FIFOF#(Fwd_channel#(a,w,o,i,op,u)) f_fwd_channel <- mkGFIFOF (unguarded, guarded);
    FIFOF#(Resp_channel#(a,w,o,i,op,acks,u)) f_resp_channel <- mkGFIFOF (unguarded, guarded);
    

    interface shaktilink_side = interface Ifc_slc_master

      // interface declaration for Req_Channel
      method Bit#(op) m_req_opcode = f_req_channel.first.opcode; 
      method Bit#(8) m_req_len = f_req_channel.first.len;		                           
      method Bit#(3) m_req_size = f_req_channel.first.size;			                       
      method Bit#(1) m_req_tid = f_req_channel.first.tid;			                       
      method Bit#(o) m_req_source = f_req_channel.first.source; 
      method Bit#(i) m_req_dest = f_req_channel.first.dest; 
      method Bit#(a) m_req_address = f_req_channel.first.address; 
      method Bit#(w) m_req_mask = f_req_channel.first.mask; 
      method Bit#(TMul#(w,8)) m_req_data = f_req_channel.first.data; 
      method Bit#(u) m_req_user = f_req_channel.first.user;
      method Bool m_req_valid = f_req_channel.notEmpty;
      method Action m_req_ready (Bool req_ready);    // in
        if(req_ready && f_req_channel.notEmpty) f_req_channel.deq;
      endmethod
      
      // interface declaration for Fwd_Channel
      method Action m_fwd_valid(Bit#(op) fwd_opcode, Bit#(o) fwd_source, Bit#(i) fwd_dest, 
                                Bit#(a) fwd_address, Bit#(w) fwd_mask, Bit#(TMul#(w,8)) fwd_data, 
                                Bit#(1) fwd_tid, Bit#(u) fwd_user, Bool fwd_valid);
        if(fwd_valid && f_fwd_channel.notFull)
          f_fwd_channel.enq(Fwd_channel{
                                        opcode:  fwd_opcode, 
                                        source:  fwd_source, 
                                        dest:    fwd_dest, 
                                        address: fwd_address, 
                                        mask:    fwd_mask, 
                                        data:    fwd_data,
                                        tid:     fwd_tid,
                                        user:    fwd_user});
      endmethod
      method m_fwd_ready = f_fwd_channel.notFull; 

      // interface declaration for Response_Channel
      method Action m_resp_valid (
          Bit#(op) resp_opcode, Bit#(acks) resp_acksExpected, Bool  resp_last, Bit#(o) resp_source, 
          Bit#(i) resp_dest, Bit#(a) resp_address, Bit#(TMul#(w,8)) resp_data, Bit#(1) resp_tid,
          Bit#(1) resp_corrupt, Bit#(u) resp_user, Bool resp_valid );
        if(resp_valid && f_resp_channel.notFull)
          f_resp_channel.enq(Resp_channel{
                                          opcode:      resp_opcode, 
                                          acksExpected:resp_acksExpected,
                                          last:        resp_last, 
                                          source:      resp_source, 
                                          dest:        resp_dest, 
                                          address:     resp_address, 
                                          data:        resp_data, 
                                          tid:         resp_tid,
                                          corrupt:     resp_corrupt,
                                          user:        resp_user});
      endmethod
      method m_resp_ready = f_resp_channel.notFull;    
    endinterface;

    interface i_req_channel = to_FIFOF_I(f_req_channel);
    interface o_fwd_channel = to_FIFOF_O(f_fwd_channel);
    interface o_resp_channel= to_FIFOF_O(f_resp_channel);
  endmodule

  interface Ifc_slc_slave_agent#(numeric type a, numeric type w, numeric type o, 
                            numeric type i, numeric type op, numeric type acks, numeric type u);

    interface Ifc_slc_slave#(a,w,o,i,op,acks,u) shaktilink_side;
    // FIFOF side
    interface FIFOF_I #(Resp_channel#(a,w,o,i,op,acks,u))        i_resp_channel;
    interface FIFOF_I #(Fwd_channel#(a,w,o,i,op,u))              i_fwd_channel;
    interface FIFOF_O #(Req_channel#(a,w,o,i,op,u))              o_req_channel;

  endinterface: Ifc_slc_slave_agent

  module mkslc_slave_agent (Ifc_slc_slave_agent#(a,w,o,i,op,acks,u));

    Bool unguarded = True;
    Bool guarded   = False;

    // These FIFOs are guarded on BSV side, unguarded on AXI side
    FIFOF#(Req_channel#(a,w,o,i,op,u)) f_req_channel <- mkGFIFOF (unguarded, guarded);
                               
    FIFOF#(Resp_channel#(a,w,o,i,op,acks,u)) f_resp_channel <- mkGFIFOF (guarded, unguarded);
    FIFOF#(Fwd_channel#(a,w,o,i,op,u)) f_fwd_channel <- mkGFIFOF (guarded, unguarded);

    interface o_req_channel = to_FIFOF_O(f_req_channel);
    interface i_fwd_channel = to_FIFOF_I(f_fwd_channel);
    interface i_resp_channel= to_FIFOF_I(f_resp_channel);
    interface shaktilink_side = interface Ifc_slc_slave
      // interface declaration for Req_Channel
      method Action s_req_valid(Bit#(op) req_opcode, Bit#(8) req_len, Bit#(3) req_size,
                                Bit#(o) req_source, Bit#(i) req_dest, 
                                Bit#(a) req_address, Bit#(w) req_mask, 
                                Bit#(TMul#(w,8)) req_data, Bit#(1) req_tid, Bit#(u) req_user, Bool req_valid);
        if(req_valid && f_req_channel.notFull)
          f_req_channel.enq(Req_channel{
                                        opcode: req_opcode, 
                                        len:		req_len,		                           
                                        size:		req_size,			                       
                                        source: req_source, 
                                        dest:   req_dest, 
                                        address:req_address, 
                                        mask:   req_mask, 
                                        data:   req_data, 
                                        tid:    req_tid,
                                        user:   req_user});
      endmethod
      method s_req_ready = f_req_channel.notFull;
      
      
      // interface declaration for Fwd_Channel
      method Bit#(op) s_fwd_opcode = f_fwd_channel.first.opcode; 
      method Bit#(o) s_fwd_source = f_fwd_channel.first.source; 
      method Bit#(i) s_fwd_dest = f_fwd_channel.first.dest; 
      method Bit#(a) s_fwd_address = f_fwd_channel.first.address; 
      method Bit#(w) s_fwd_mask = f_fwd_channel.first.mask; 
      method Bit#(TMul#(w,8)) s_fwd_data = f_fwd_channel.first.data; 
      method Bit#(1) s_fwd_tid = f_fwd_channel.first.tid; 
      method Bit#(u) s_fwd_user = f_fwd_channel.first.user;
      method Bool s_fwd_valid = f_fwd_channel.notEmpty;
      method Action s_fwd_ready (Bool fwd_ready);    // in
        if(fwd_ready && f_fwd_channel.notEmpty) f_fwd_channel.deq;
      endmethod

      // interface declaration for Response_Channel
      method Bit#(op) s_resp_opcode = f_resp_channel.first.opcode; 
      method Bit#(acks) s_resp_acksExpected = f_resp_channel.first.acksExpected;
      method Bool    s_resp_last = f_resp_channel.first.last; 
      method Bit#(o) s_resp_source = f_resp_channel.first.source; 
      method Bit#(i) s_resp_dest = f_resp_channel.first.dest; 
      method Bit#(a) s_resp_address = f_resp_channel.first.address; 
      method Bit#(TMul#(w,8)) s_resp_data = f_resp_channel.first.data; 
      method Bit#(1) s_resp_tid = f_resp_channel.first.tid; 
      method Bit#(1) s_resp_corrupt = f_resp_channel.first.corrupt;
      method Bit#(u) s_resp_user = f_resp_channel.first.user; 
      method Bool s_resp_valid  = f_resp_channel.notEmpty;
      method Action s_resp_ready (Bool resp_ready);
        if(resp_ready && f_resp_channel.notEmpty) f_resp_channel.deq;
      endmethod
    endinterface;
  endmodule

endpackage

