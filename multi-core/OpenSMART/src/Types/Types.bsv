import Vector::*;

//User parameters
//typedef 100000  BenchmarkCycle;
typedef 100000  BenchmarkCycle;

typedef 132      DataSz;
typedef 1       NumFlitsPerDataMessage;
typedef 1       NumFlitsPerControlMessage;

//Number of VNETs
//criticality parameters
typedef 1       HIperVNET; //specify the number of HI VCs in one VNET.
typedef HIperVNET       LOperVNET; //specify the number of LO VCs in one VNET.
//NOTE : assume HIperVNET = LOperVNET
// TODO : support for asymmetric HIperVNET and LOperVNET
typedef TAdd#(HIperVNET, LOperVNET) VCperVNET;
typedef 2       CritLevels;

typedef 4       NumVNETs;
typedef 6       UserHPCMax;

typedef 4       MeshWidth;
typedef 4       MeshHeight;
typedef TMul#(NumVNETs, VCperVNET)       NumUserVCs;
// This value should be required injection ratio*127.
// If you want 20% injection rate the following variable should be set to ceil(0.2x127) = 26.
typedef 25     InjectionRate;

// to set the depth of queueing buffer (garnet has infinite queueing depth)
// if there is no space to queue(tempFifo in TrafficGeneratorUnit),
// a packet won't get injected regardless of injection rate.
typedef 4      NumTrafficGeneratorBufferSlots;

RoutingAlgorithms currentRoutingAlgorithm = XY_;

///////////////////////////////////////////////////////////////
//Fixed and derived Types

typedef enum {XY_, YX_} RoutingAlgorithms deriving(Bits, Eq);
typedef TMul#(MeshWidth, MeshHeight) NumMeshNodes;

typedef	Bit#(DataSz) Data;

//Dimensions, fixed for mesh network
typedef 5                  NumPorts;       //N, E, S, W, L
typedef TSub#(NumPorts, 1) NumNormalPorts; //N, E, S, W

typedef NumPorts           MaxNumPorts;    //For arbitrary topology
typedef NumNormalPorts     MaxNumNormalPorts;

//Mesh dimensions
typedef	TAdd#(1, TLog#(MeshWidth))	MeshWidthBitSz;
typedef	TAdd#(1, TLog#(MeshHeight))	MeshHeightBitSz;

typedef	Bit#(MeshWidthBitSz)	MeshWIdx;
typedef	Bit#(MeshHeightBitSz)	MeshHIdx;

typedef struct {
	MeshWIdx x_len;
	MeshHIdx y_len;
} Len deriving(Bits, Eq, FShow);

interface NtkArbiter#(numeric type numRequesters);
  method Action                            initialize;
  method ActionValue#(Bit#(numRequesters)) getArbit(Bit#(numRequesters) reqBit);
endinterface
