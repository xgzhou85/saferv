package NIC_interface;

import Types ::*;
import MessageTypes ::*;
import CreditTypes ::*;

interface NetworkOuterInterface;
  method Action                     putFlit(Flit flit);
  method ActionValue#(Flit)         getFlit;
  method Action                     putCredit(CreditSignal crd);
  method ActionValue#(CreditSignal) getCredit;
endinterface

interface StatsOuter;
		method Action incSendCount(MsgType msg);
		method Action incRecvCount(MsgType msg);
		method Action incLatencyCount(Data latency);
		method Action incHopCount(Data hopCount);
		method Action incInflightLatencyCount(Data latency);
endinterface

//interface NIC;
//		interface NetworkOuterInterface mesh ;
//		interface StatsOuter stats;
//endinterface

endpackage
