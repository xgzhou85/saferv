import Types::*;
import VirtualChannelTypes::*;
import MessageTypes::*;
//Credits
typedef TMax#(ControlVCDepth, DataVCDepth)    MaxCreditCount;
typedef Bit#(TAdd#(1, TLog#(MaxCreditCount))) Credit;
typedef TMin#(ControlVCDepth, DataVCDepth)    InitialCredit;

typedef struct {
  VCIdx vc;
  Bool isTailFlit;
  MsgType msgType;
  CritType critType;
} CreditSignal_ deriving(Bits, Eq, FShow);

typedef Maybe#(CreditSignal_) CreditSignal;

