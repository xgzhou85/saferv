import Vector::*;
import Fifo::*;
import CReg::*;
import FIFO::*;
import FIFOF::*;
import SpecialFIFOs::*;
import Types::*;
import MessageTypes::*;
import SwitchAllocTypes::*;
import RoutingTypes::*;


interface CrossbarBufferChannel;
  method Action putFlit(Flit flit);
endinterface

interface CrossbarBuffer;
  method ActionValue#(Flit) getFlit;
  interface Vector#(NumPorts, CrossbarBufferChannel) bufferChannel;
endinterface

(* synthesize *)
module mkCrossbarBuffer(CrossbarBuffer);

  CReg#(TAdd#(NumPorts, 1), Maybe#(Flit)) buffer <- mkCReg(Invalid);
	FIFOF#(Flit) actual_buff <- mkBypassFIFOF;

	rule rl_enq_actbuff(isValid(buffer[valueOf(NumPorts)]));
			actual_buff.enq(validValue(buffer[valueOf(NumPorts)]));
	endrule

	Vector#(NumPorts, CrossbarBufferChannel) bufferChannelDummy;
  for(Integer prt=0; prt<valueOf(NumPorts); prt=prt+1) begin
    bufferChannelDummy[prt] = 
      interface CrossbarBufferChannel
        method Action putFlit(Flit flit) if(actual_buff.notFull);
          buffer[prt] <= Valid(flit);
					//act_buff.enq(buffer[valueOf(NumPorts)]);
        endmethod
      endinterface;
  end

  interface bufferChannel = bufferChannelDummy;

  method ActionValue#(Flit) getFlit if(isValid(buffer[valueOf(NumPorts)]));
    buffer[valueOf(NumPorts)] <= Invalid;
		actual_buff.deq;
    //return validValue(buffer[valueOf(NumPorts)]);
		return actual_buff.first;
  endmethod

endmodule

