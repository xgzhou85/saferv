import Types::*;
import DReg::*;

interface Clock;
  method Reg#(Data) getClkCount;
endinterface

(* synthesize *)
module mkClock(Clock);
  Reg#(Data) clkCount  <- mkReg(0);
  
  rule doClkCount(clkCount < fromInteger(valueOf(BenchmarkCycle)));
    if(clkCount % 10000 == 0) begin
      $display("Elapsed Simulation Cycles: %d",clkCount);
    end
    clkCount <= clkCount + 1;
  endrule

  method Reg#(Data) getClkCount = clkCount;
endmodule