import Vector::*;
import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;


interface MonitorStats;
  method Vector#(MeshHeight, Vector#(MeshWidth, Vector#(NumVCs, Data))) getSendCount;
  method Vector#(MeshHeight, Vector#(MeshWidth, Vector#(NumVCs, Data))) getRecvCount;
  (*always_ready, always_enabled*)
  method Action monitor_ingress( Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) arg1);
  (*always_ready, always_enabled*)
  method Action monitor_egress( Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) arg2);
   method Action printFinalStats;
endinterface

(*synthesize*)
module mkMonitorStats(MonitorStats);

/******************** states to capture monitoring wires **************/

Vector#(MeshHeight, Vector#(MeshWidth, Vector#(NumVCs, Reg#(Data)))) send_count <- replicateM(replicateM(replicateM(mkReg(0))));
  Vector#(MeshHeight, Vector#(MeshWidth, Vector#(NumVCs, Reg#(Data)))) recv_count <- replicateM(replicateM(replicateM(mkReg(0))));

 method Action monitor_ingress( Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) arg1);
    //count the packets injected for every node.
    let lv_count = arg1;
    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
          if(lv_count[i][j][k])begin
            send_count[i][j][k] <= send_count[i][j][k] +1 ;
          end
        end
      end
    end
  endmethod

  method Action monitor_egress( Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) arg2);
   //count the packets ejected for every node.
   let lv_count = arg2;
   for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
          if(lv_count[i][j][k])begin
            recv_count[i][j][k] <= recv_count[i][j][k] + 1;
          end
        end
      end
    end
  endmethod

  method Vector#(MeshHeight, Vector#(MeshWidth, Vector#(NumVCs, Data)))  getSendCount;
    //send the count of injected packets
    Vector#(MeshHeight, Vector#(MeshWidth, Vector#(NumVCs, Data))) lv_send = ?;

    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
          lv_send[i][j][k] = send_count[i][j][k];
        end
      end
    end

    return lv_send;
  endmethod

  method Vector#(MeshHeight, Vector#(MeshWidth, Vector#(NumVCs, Data)))  getRecvCount;
    //send the count of ejected packets
    Vector#(MeshHeight, Vector#(MeshWidth, Vector#(NumVCs, Data))) lv_recv = ? ;

    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
          lv_recv[i][j][k] = recv_count[i][j][k];
        end
      end
    end

    return lv_recv;
  endmethod

  method Action printFinalStats;
    //print statistics
    // total packets injected
    // total packets ejected
    // total packets injected and ejected at every node.

    let lv_send_count = send_count;
    let lv_recv_count = recv_count;
    Vector#(MeshHeight, Vector#(MeshWidth, Bit#(DataSz))) stat_send = replicate(replicate(0));
    Vector#(MeshHeight, Vector#(MeshWidth, Bit#(DataSz))) stat_recv = replicate(replicate(0));

    Bit#(DataSz) total_inj_packets = 0;
    Bit#(DataSz) total_recv_packets = 0;
    Bit#(DataSz) total_recv_packets_hi = 0;
    Bit#(DataSz) total_recv_packets_lo = 0;

    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
        for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
          for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
            $display("MONITOR Injected in Router[%2d][%2d] in VC: %d is : %d", i,j,k, lv_send_count[i][j][k]);
            stat_send[i][j] = stat_send[i][j] + lv_send_count[i][j][k];
            total_inj_packets = total_inj_packets + lv_send_count[i][j][k];

          end
        end
      end

      for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
        for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
          for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
            $display("MONITOR Eejected in Router[%2d][%2d] in VC: %d is : %d", i,j,k, lv_recv_count[i][j][k]);
            stat_recv[i][j] = stat_recv[i][j] + lv_recv_count[i][j][k];
            total_recv_packets = total_recv_packets +  lv_recv_count[i][j][k];

            if(k%2==0)
              begin
                total_recv_packets_hi = total_recv_packets_hi + lv_recv_count[i][j][k];
              end
            else
              begin
                total_recv_packets_lo = total_recv_packets_lo + lv_recv_count[i][j][k];
              end

          end
        end
      end

      for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
        for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
          $display("MONITOR: Injected by ROUTER[%2d][%2d] in : %2d],   Ejected by ROUTER[%2d][%2d] from: %d", i, j, stat_send[i][j], i, j, stat_recv[i][j]);
        end
      end


      $display("MONITOR  :   Total injected packet: %d",total_inj_packets);
      $display("MONITOR  :  Total received packet: %d",total_recv_packets);
      $display("MONITOR  :  Total HI received packet: %d",total_recv_packets_hi);
      $display("MONITOR  :  Total LO received packet: %d",total_recv_packets_lo);

endmethod

endmodule
