import Vector::*;
import Fifo::*;
import CReg::*;

import Types::*;
import MessageTypes::*;
import SwitchAllocTypes::*;
import RoutingTypes::*;

import CrossbarBuffer::*;

interface CrossbarPort;
  method Action putFlit(Flit traverseFlit, DirIdx destDirn);
  method ActionValue#(Flit) getFlit;
endinterface

interface CrossbarSwitch;
  interface Vector#(NumPorts, CrossbarPort) crossbarPorts;
endinterface

//TODO :- Try to make buffer channel a single interface which helps write into a normal Reg and not a CReg.
// buffer channel is a vector of interface with value being written in a CReg, as we can't put conflict free attributes among the methods. Each put flit method by design will have different destdirn.
(* synthesize *)
module mkCrossbarSwitch(CrossbarSwitch);

  Vector#(NumPorts, CrossbarBuffer) outBuffer <- replicateM(mkCrossbarBuffer);

  Vector#(NumPorts, CrossbarPort) crossbarPortsDummy;
  for(Integer prt = 0; prt < valueOf(NumPorts); prt = prt+1) begin
    crossbarPortsDummy[prt] =
      interface CrossbarPort

        //Input side
        method Action putFlit(Flit traverseFlit, DirIdx destDirn);
          outBuffer[destDirn].bufferChannel[prt].putFlit(traverseFlit);
        endmethod

        //Output side
        method ActionValue#(Flit) getFlit;
          let ret <- outBuffer[prt].getFlit;
          return ret;
        endmethod
      endinterface;
  end
  interface crossbarPorts = crossbarPortsDummy;

endmodule
