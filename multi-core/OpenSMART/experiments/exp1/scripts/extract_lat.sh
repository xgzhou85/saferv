 #!/bin/bash
# This script runs OpenSMART for a range of values and extracts the value of
# total latency, total packets injected, total packets received into a text file.
# NOTE : set the value of Injection Rate as 3 in src/Types/Types.bsv in the
# beginning
echo > log_total_lat.txt
echo > log_total_inj_packets.txt
echo > log_total_recv_packets.txt
b=3
y=3
for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
#for i in 1 2 3
do
    x=$(echo "$i * $b" | bc )
    echo $x
    cd ../../../src/Types/
    sed -i "s/typedef $y     InjectionRate;/typedef $x     InjectionRate;/g" Types.bsv
    y=$x
    cd ../../
    str1="make generate_verilog link_verilator" # command for make generate_verilog link_verilator
    str2="./bin/sim +fullverbose > log" # command for ./bin/sim +fullverbose
    eval "$str1"
    eval "$str2"
    grep "Total latency:" log | sed 's/Total latency:s*//' >> ./experiments/exp1/scripts/log_total_lat.txt
    grep "Total injected packet:" log | sed 's/Total injected packet:s*//' >> ./experiments/exp1/scripts/log_total_inj_packets.txt

    grep "Total received packet:" log | sed 's/Total received packet:s*//' >> ./experiments/exp1/scripts/log_total_recv_packets.txt
		cd ./experiments/exp1/scripts
done

