
package TL_testbench;
import Vector::*;
import GetPut ::*;
import Fifo::*;
import Connectable::*;


import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;

import Network::*;
import TrafficGeneratorUnit::*;
import TrafficGeneratorBuffer::*;
import CreditUnit::*;
//import StatLogger::*;
import MonitorStats::*;

import NIC_interface ::*;
import Tilelink_Types ::*;
import Tilelink_NIC::*;
import Clock::*;
//import Network::*;

`include "system.defines"

interface Ifc_TL_TestBench;

  `ifdef  monitor_link_utilisation
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) monitor_link_utilisation;
  `endif
  // `ifdef monitor_hop_counts // encode hop count as destination id of outgoing flit. id = node_id implies no flit.
  //   (* always_ready *)
  //   method Vector#(MeshHeight,Vector#(MeshWidth,Bit#(TLog#(Mul#(MeshHeight,MeshWidth))))) monitor_traffic_latency;
  // `endif
  `ifdef monitor_flit_ingress
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_ingress;
  `endif
  `ifdef monitor_flit_egress
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_egress;
  `endif
endinterface

(* synthesize *)
module mkTL_testBench(Ifc_TL_TestBench);

  /********************************* States *************************************/
  Clock clkCount  <- mkClock;
  Reg#(Bool) inited    <- mkReg(False);
  Reg#(Data) initCount <- mkReg(0);


  Vector#(MeshHeight, Vector#(MeshWidth, Ifc_Master_link#(`PADDR, `LANE_WIDTH, `BURST_SIZE)))    m_xactor
																								<- replicateM(replicateM(mkMasterXactor(True, True)));
  Vector#(MeshHeight, Vector#(MeshWidth, Ifc_Slave_link#(`PADDR, `LANE_WIDTH, `BURST_SIZE)))    s_xactor
																								<- replicateM(replicateM(mkSlaveXactor(True, True)));

  Vector#(MeshHeight, Vector#(MeshWidth, ReverseCreditUnit))  creditUnits    <- replicateM(replicateM(mkReverseCreditUnit));
  //Vector#(MeshHeight, Vector#(MeshWidth, StatLogger))         statLoggers    <- replicateM(replicateM(mkStatLogger));

  MonitorStats monitorStats <- mkMonitorStats;
  /******************************** Submodule ************************************/

  Network meshNtk <- mkNetwork;
  // connect the monitor wires with monitorStats
  mkConnection(monitorStats.monitor_ingress, meshNtk.monitor_node_ingress);
  mkConnection(monitorStats.monitor_egress, meshNtk.monitor_node_egress);

  rule init(!inited);
    if(initCount == 0) begin
      //clkCount <= 0;
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        // initializing the source node for traffic
				m_xactor[0][j].core_side.coords(Len {x_len:fromInteger(j), y_len:0});
				s_xactor[valueOf(MeshHeight)-1][j].core_side.coords(Len {x_len:fromInteger(j), y_len:fromInteger(valueOf(MeshHeight))-1});
       //`ifdef LFSR
       // trafficGeneratorUnits[i][j].initialize(fromInteger(i), fromInteger(j), fromInteger(k));
       // `else
       // trafficGeneratorUnits[i][j].initialize(fromInteger(i), fromInteger(j));
       // `endif
      end
    end

    initCount <= initCount + 1;
    if(meshNtk.isInited && initCount > fromInteger(valueOf(MeshHeight)) + fromInteger(valueOf(MeshWidth)))  begin
      inited <= True;
    end
  endrule

  /*
  rule doClkCount(inited && clkCount < fromInteger(valueOf(BenchmarkCycle)));
    if(clkCount % 10000 == 0) begin
      $display("Elapsed Simulation Cycles: %d",clkCount);
    end
    clkCount <= clkCount + 1;
  endrule
*/

	rule rl_send_req_through_tilelink(inited && clkCount.getClkCount < 10000 && clkCount.getClkCount %3==0);
		A_channel#(`PADDR, `LANE_WIDTH, `BURST_SIZE) packet = A_channel {a_opcode : Acquire,
																	a_param : 0,
																	a_size : 3,
																	a_source : 2,
																	a_address : 'h82000000,
																	a_mask : '1,
																	a_data : 0};
    m_xactor[0][0].core_side.master_req_A.put(packet);
    m_xactor[0][valueOf(MeshHeight)-1].core_side.master_req_A.put(packet);
    
	endrule

  rule rl_recv_resp_D;
  //The channel D is responsible for the slave responses. It has the master ids and slave ids carried through the channel

    let d_packet <- m_xactor[0][valueOf(MeshHeight)-1].core_side.master_resp_D.get();
    //$display("The address sent %h", packet.a_address);

	endrule

	rule rl_resp_from_tilelink;
		let packet <- s_xactor[valueOf(MeshHeight)-1][0].core_side.slave_req_A.get();

  endrule

  rule rl_send_D_resp_through_tilelink(inited && clkCount.getClkCount < 10000 && clkCount.getClkCount %3==0);
    D_channel#(`LANE_WIDTH, `BURST_SIZE) d_packet = D_channel{
            d_opcode: AccessAck,                     //Opcode encodings for responses with data or just ack
            d_param: 0,
            d_size:3,
            d_source: 2,
            d_sink: 1,
            d_data: 0,
            d_error: False};

    s_xactor[valueOf(MeshHeight)-1][valueOf(MeshHeight)-1].core_side.slave_resp_D.put(d_packet);
    s_xactor[valueOf(MeshHeight)-1][0].core_side.slave_resp_D.put(d_packet);
    //$display("The address received %h", packet.a_address);
	endrule

  rule finishBench(inited && clkCount.getClkCount == fromInteger(valueOf(BenchmarkCycle)));
     // print the STATS.
     // TODO : vnet, criticality, and tilelink specific stats can be added here.
     let hi_total_lat = meshNtk.latencyStats[0].getLatencyCount;
     let lo_total_lat = meshNtk.latencyStats[1].getLatencyCount;

      let hi_max = meshNtk.latencyStats[0].maxLatency;
      let lo_max = meshNtk.latencyStats[1].maxLatency;
     $display("Total LO latency is: %d", lo_total_lat);
     $display("Total HI latency is: %d", hi_total_lat);
     $display("\n Maximum LO latency is: %d", lo_max);
     $display("Maximum HI latency is: %d\n", hi_max);
     monitorStats.printFinalStats;
     $finish;
  endrule


  //Credit Links
  //for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
  //  for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin

  //    mkConnection(creditUnits[i][j].getCredit,
  //                   meshNtk.ntkPorts[i][j].putCredit);

  //    mkConnection(meshNtk.ntkPorts[i][j].getCredit,
  //                   trafficGeneratorUnits[i][j].putVC);
  //  end
  //end

//	interface Vector#(MeshHeight, Vector#(MeshWidth, NIC)) outer;
//	for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
//			for(Integer i=0; i<valueOf(MeshWidth); i=i+1) begin
//					outer[i][j].mesh = meshNtk.ntkPorts[i][j];
//					outer[i][j].stats = statLoggers[i][j].outer;
//			end
//	end

	for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
		mkConnection(m_xactor[0][i].fabric_side, meshNtk.ntkPorts[0][i]);
		mkConnection(meshNtk.ntkPorts[valueOf(MeshHeight)-1][i], s_xactor[valueOf(MeshHeight)-1][i].fabric_side);
	end

//	for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
//			mkConnection(m_xactor[i].fabric_side, outer[0][i]);
//			mkConnection(outer[1][i], s_xactor[i].fabric_side);
//	end

  `ifdef  monitor_link_utilisation
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) monitor_link_utilisation = meshNtk.monitor_link_utilisation;
  `endif
  // `ifdef monitor_hop_counts // encode hop count as destination id of outgoing flit. id = node_id implies no flit.
  //   (* always_ready *)
  //   method Vector#(MeshHeight,Vector#(MeshWidth,Bit#(TLog#(Mul#(MeshHeight,MeshWidth))))) monitor_traffic_latency;
  // `endif
  `ifdef monitor_flit_ingress
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_ingress = meshNtk.monitor_node_ingress;
  `endif
  `ifdef monitor_flit_egress
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_egress = meshNtk.monitor_node_egress;
  `endif


endmodule
endpackage : TL_testbench
