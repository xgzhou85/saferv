package TG_TestBench;
import Vector::*;
import Fifo::*;
import Connectable::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;
import NIC_interface::*;


import Network::*;
import TrafficGeneratorUnit::*;
import TrafficGeneratorBuffer::*;
import CreditUnit::*;
import StatLogger::*;


interface Ifc_TG_TestBench;
  `ifdef  monitor_link_utilisation
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) monitor_link_utilisation;
  `endif
  // `ifdef monitor_hop_counts // encode hop count as destination id of outgoing flit. id = node_id implies no flit.
  //   (* always_ready *)
  //   method Vector#(MeshHeight,Vector#(MeshWidth,Bit#(TLog#(Mul#(MeshHeight,MeshWidth))))) monitor_traffic_latency;
  // `endif
  `ifdef monitor_flit_ingress
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_ingress;
  `endif
  `ifdef monitor_flit_egress
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_egress;
  `endif
endinterface


//(* synthesize *)
module mkTG_TestBench(Ifc_TG_TestBench);

  /********************************* States *************************************/
  Reg#(Data) clkCount  <- mkReg(0);
  Reg#(Bool) inited    <- mkReg(False);
  Reg#(Data) initCount <- mkReg(0);
  Reg#(Data) rg_hi_max <- mkReg(0);
  Reg#(Data) rg_lo_max <- mkReg(0);

  Vector#(MeshHeight, Vector#(MeshWidth, TrafficGeneratorUnit))    trafficGeneratorUnits       <- replicateM(replicateM(mkTrafficGeneratorUnit));
  Vector#(MeshHeight, Vector#(MeshWidth, TrafficGeneratorBuffer))  trafficGeneratorBufferUnits <- replicateM(replicateM(mkTrafficGeneratorBuffer));

  Vector#(MeshHeight, Vector#(MeshWidth, ReverseCreditUnit))  creditUnits    <- replicateM(replicateM(mkReverseCreditUnit));
  Vector#(MeshHeight, Vector#(MeshWidth, StatLogger))         statLoggers    <- replicateM(replicateM(mkStatLogger));
  Vector#(MeshHeight, Vector#(MeshWidth, Vector#(NumVCs, Reg#(Data)))) send_count <- replicateM(replicateM(replicateM(mkReg(0))));
  Vector#(MeshHeight, Vector#(MeshWidth, Vector#(NumVCs, Reg#(Data)))) recv_count <- replicateM(replicateM(replicateM(mkReg(0))));


  /******************************** Submodule ************************************/

  Network meshNtk <- mkNetwork;

  rule init(!inited);
    if(initCount == 0) begin
      clkCount <= 0;
      Integer k=1;
      for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
        for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
          k=k+1;
          // initializing the source node for traffic
         `ifdef LFSR
          trafficGeneratorUnits[i][j].initialize(fromInteger(i), fromInteger(j), fromInteger(k));
          `else
          trafficGeneratorUnits[i][j].initialize(fromInteger(i), fromInteger(j));
          `endif
        end
      end
    end

    initCount <= initCount + 1;
    if(meshNtk.isInited && initCount > fromInteger(valueOf(MeshHeight)) + fromInteger(valueOf(MeshWidth)))  begin
      inited <= True;
    end
  endrule

  rule doClkCount(inited && clkCount < fromInteger(valueOf(BenchmarkCycle)));
    if(clkCount % 10000 == 0) begin
      $display("Elapsed Simulation Cycles: %d",clkCount);
    end
    clkCount <= clkCount + 1;
  endrule


/*
  //count the packets injected for every node
  rule increment_send_count;

    let lv_count = meshNtk.monitor_node_ingress;
    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
          if(lv_count[i][j][k])begin
            send_count[i][j][k] <= send_count[i][j][k] +1 ;
          end
        end
      end
    end
  endrule

  //count the packets ejected for every node
  rule increment_recv_count;

  let lv_count = meshNtk.monitor_node_egress;
   for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
          if(lv_count[i][j][k])begin
            recv_count[i][j][k] <= recv_count[i][j][k] + 1;
          end
        end
      end
    end

  endrule
*/

  rule finishBench(inited && clkCount == fromInteger(valueOf(BenchmarkCycle)));
      Vector#(CritLevels, Data) res = ?;
      Vector#(CritLevels, Data) send = ?;
      for (Integer crit = 0; crit < fromInteger(valueOf(CritLevels)); crit = crit+1)begin
        send[crit] = 0;
        res[crit] = 0;
      end

      Data hop = 0;
      Data remainingFlits = 0;
      Data inflight = 0;
      Vector#(CritLevels, Bit#(128)) resLatency = ?;
      for(Integer crit = 0; crit < fromInteger(valueOf(CritLevels)); crit = crit+1)begin
          resLatency[crit] = 0;
        end

			Vector#(CritLevels, Vector#(NumVNETs, Data)) sendCount = ?;
			Vector#(CritLevels, Vector#(NumVNETs, Data)) recvCount = ?;
      for(Integer crit = 0; crit < fromInteger(valueOf(CritLevels)); crit = crit+1)begin
       for(Integer vnet = 0; vnet < fromInteger(valueOf(NumVNETs)); vnet = vnet +1)begin
        sendCount[crit][vnet] = 0;
        recvCount[crit][vnet] = 0;
      end
      end



      for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
        for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
          sendCount = statLoggers[i][j].getSendCount;
          recvCount = statLoggers[i][j].getRecvCount;
          Vector#(CritLevels, Data) latencyCount = statLoggers[i][j].getLatencyCount;
          Data remFlits = trafficGeneratorBufferUnits[i][j].getRemainingFlitsNumber;
//          Data extraLatency = trafficGeneratorBufferUnits[i][j].getExtraLatency(clkCount);
          Data hopCount = statLoggers[i][j].getHopCount;
          Data inflightCycle = statLoggers[i][j].getInflightLatencyCount;
					//for(Integer k=0; k<valueOf(NumVNETs); k=k+1) begin
							//$display("send_count[%2d][%2d] VNET0 = %5d  VNET1 = %5d  VNET2 = %5d,   recv_count[%2d][%2d] VNET0 = %5d  VNET1 = %5d  VNET2 = %5d",
					//i, j, sendCount[0], sendCount[1],sendCount[2], i, j, recvCount[0], recvCount[1], recvCount[2]);

      for(Integer crit = 0; crit < fromInteger(valueOf(CritLevels)); crit = crit+1)begin
        for(Integer vnet = 0; vnet < fromInteger(valueOf(NumVNETs)); vnet = vnet +1)begin
           $display("Injected by ROUTER[%2d][%2d] in Crit[%2d], in VNET[%2d]: %5d],   Ejected by ROUTER[%2d][%2d] from  VNET[%2d]: %5d", i, j, crit, vnet,sendCount[crit][vnet], i, j, vnet, recvCount[crit][vnet]);
						send[crit] = send[crit] + sendCount[crit][vnet];
						res[crit] = res[crit] + recvCount[crit][vnet];
				end
      end

          //resLatency = resLatency + zeroExtend(latencyCount);// + zeroExtend(extraLatency);
          for(Integer crit = 0; crit < fromInteger(valueOf(CritLevels)); crit = crit+1)begin
            resLatency[crit] = resLatency[crit] + zeroExtend(latencyCount[crit]);
          end
          remainingFlits = remainingFlits + remFlits;
          hop = hop + hopCount;
          inflight = inflight + inflightCycle;
	    end
      end

      // collecting statistics from the monitoring wires
      // ingress and egress

      //Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Data))) lv_send_count;
      //Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Data))) lv_recv_count;
/*
      let lv_send_count = send_count;
      let lv_recv_count = recv_count;
      Vector#(MeshHeight, Vector#(MeshWidth, Bit#(128))) stat_send = ?;
      Vector#(MeshHeight, Vector#(MeshWidth, Bit#(128))) stat_recv = ?;

      Bit#(128) total_inj_packets = 0;
      Bit#(128) total_recv_packets = 0;

      for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
        for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
          for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
            $display("MONITOR Injected in Router[%2d][%2d] in VC: %d is : %d", i,j,k,lv_send_count[i][j][k]);
            stat_send[i][j] = stat_send[i][j] + lv_send_count[i][j][k];
            total_inj_packets = total_inj_packets + lv_send_count[i][j][k];
          end
        end
      end

      for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
        for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
          for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
            $display("MONITOR Eejected in Router[%2d][%2d] in VC: %d is : %d", i,j,k,                    lv_recv_count[i][j][k]);
            stat_recv[i][j] = stat_recv[i][j] + lv_recv_count[i][j][k];
            total_recv_packets = total_recv_packets +  lv_recv_count[i][j][k];
          end
        end
      end

      for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
        for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
          $display("MONITOR: Injected by ROUTER[%2d][%2d] in : %2d],   Ejected by ROUTER[%2d][%2d]     from: %d", i, j, stat_send[i][j], i, j, stat_recv[i][j]);
        end
      end

*/


			//Vector#(NumVNETs, Data) sendCount = ?;
			//Vector#(NumVNETs, Data) recvCount = ?;
			//for(Integer k=0; k<valueOf(NumVNETs); k=k+1) begin
			//		sendCount[k] = 0;
			//		recvCount[k] = 0;
			//end

			//for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
			//		for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
			//				for(Integer k=0; j<valueOf(NumVNETs); k=k+1) begin
			//						sendCount[k] = sendCount[k] + statLoggers[i][j].getSendCount[k];
			//						recvCount[k] = recvCount[k] + statLoggers[i][j].getRecvCount[k];
			//				end
			//		end
			//end

			//for(Integer k=0; k<valueOf(NumVNETs); k=k+1) begin
			//		$display("Total injected packets for VNET %d : %d", k, sendCount[k]);
			//		$display("Total received packets for VNET %d : %d", k, recvCount[k]);
			//end

      $display("Elapsed clock cycles: %d", clkCount);
      $display("Total injected packet: %d",send[0] + send[1]);
      $display("Total received packet: %d",res[0] + res[1]);

      $display("Total HI injected packets: %d", send[0] );
      $display("Total LO injected packets: %d", send[1]);
      $display("Total HI received packets: %d", res[0]);
      $display("Total LO received packets: %d", res[1]);

      $display("Total HI latency: %d", resLatency[0]);
      $display("Total LO latency: %d", resLatency[1]);

      $display("Max HI latency: %d", rg_hi_max);
      $display("Max LO latency: %d", rg_lo_max);


      $display("Total injected packet: %d",send);
      //$display("MONITOR  :   Total injected packet: %d",total_inj_packets);
      //$display("MONITOR  :  Total received packet: %d",total_recv_packets);
      $display("Total received packet: %d",res);
      $display("Total latency: %d", resLatency);
      $display("Total hopCount: %d", hop);
      $display("Total inflight latency: %d", inflight);
      $display("Number of remaiing Flits in traffic generator side: %d", remainingFlits);
      $finish;
  endrule


  //Credit Links
  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin

      mkConnection(creditUnits[i][j].getCredit,
                     meshNtk.ntkPorts[i][j].putCredit);

      mkConnection(meshNtk.ntkPorts[i][j].getCredit,
                     trafficGeneratorUnits[i][j].putVC);
    end
  end


  //Data Links
  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin

      rule genFlits(inited);
        trafficGeneratorUnits[i][j].genFlit(clkCount);
      endrule

      rule prepareFlits(inited);
        let flit <- trafficGeneratorUnits[i][j].getFlit;
        trafficGeneratorBufferUnits[i][j].putFlit(flit);//, clkCount);
      endrule

      // TODO
      rule putFlits(inited && clkCount <30000); // inject flit into the network
        let flit <- trafficGeneratorBufferUnits[i][j].getFlit;

        `ifdef DETAILED_STATISTICS
        flit.stat.inflightCycle = clkCount;
        `endif
        meshNtk.ntkPorts[i][j].putFlit(flit);
        //$display("%10d",$time, "\tInjecting flit from [%2d,%2d] ", j,i,fshow(flit.stat));
        statLoggers[i][j].incSendCount(flit.msgType, flit.critType);
      endrule

      rule getFlits(inited);// eject flit from the network (SINK)
        let flit <- meshNtk.ntkPorts[i][j].getFlit;

        `ifdef DETAILED_STATISTICS
        Data hopCount = flit.stat.hopCount;
        `endif

//        $display($time,"\tReceiving flit from [%d,%d] ", j,i,fshow(flit.stat));
/*
        if((flit.stat.dstX != fromInteger(j)) || (flit.stat.dstY != fromInteger(i))) begin
            $display("Warning: Missrouted.\n Received from(%d, %d) but the destination is (%d, %d) source: (%d, %d)", fromInteger(i), fromInteger(j), flit.stat.dstY, flit.stat.dstX, flit.stat.srcY, flit.stat.srcX);
        end

        else begin
            $display("Correct\n Received from(%d, %d). The destination is (%d, %d)  source: (%d, %d)", fromInteger(i), fromInteger(j), flit.stat.dstY, flit.stat.dstX, flit.stat.srcY, flit.stat.srcX);
        end
*/
        `ifdef DETAILED_STATISTICS
        statLoggers[i][j].incLatencyCount(flit.critType, clkCount - flit.stat.injectedCycle);
        statLoggers[i][j].incInflightLatencyCount(clkCount - flit.stat.inflightCycle);
        statLoggers[i][j].incHopCount(hopCount);
       `endif
        statLoggers[i][j].incRecvCount(flit.msgType, flit.critType);
        $display($time, "\ttestbench calling putCredit");
        creditUnits[i][j].putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType }));
      endrule
    end
  end

  `ifdef  monitor_link_utilisation
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) monitor_link_utilisation = meshNtk.monitor_link_utilisation;
  `endif
  // `ifdef monitor_hop_counts // encode hop count as destination id of outgoing flit. id = node_id implies no flit.
  //   (* always_ready *)
  //   method Vector#(MeshHeight,Vector#(MeshWidth,Bit#(TLog#(Mul#(MeshHeight,MeshWidth))))) monitor_traffic_latency;
  // `endif
  `ifdef monitor_flit_ingress
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_ingress = meshNtk.monitor_node_ingress;
  `endif
  `ifdef monitor_flit_egress
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_egress = meshNtk.monitor_node_egress;
  `endif


endmodule
endpackage : TG_TestBench
